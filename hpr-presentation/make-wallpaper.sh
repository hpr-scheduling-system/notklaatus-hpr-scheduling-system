#!/bin/bash
rm hpr*png
if [ -e img0.png ]; then
  for file in $(seq 0 9);do 
    mv -v img${file}.png img0${file}.png 
  done
fi
for image in img*png;do 
  echo "Processing slide ${image}"
  composite -gravity east ${image} pres-sidebar.png hpr-${image}
done
rm img*png *html
#composite -gravity east img00.png aa1-screen-size.png hpr-img00.png
