#!/usr/bin/perl
# UTF-8 Test >> ÇirçösÚáóíéőöÓÁśł <<

use strict;
use warnings;
use utf8;
use Date::Format;
use Date::Calc qw( Mktime );
use Data::Dumper::Simple;
binmode STDOUT, ":utf8";

# --------------------------------------------------------------------------
# Make a list of dates that are reserved.
my $clock= 86400 + Mktime( time2str("%Y", time), time2str("%m", time), time2str("%d", time), 0, 0, 0);
# The loop should begin from the first reserved date so we catch items in the past,
# however we need to keep track of bumped down shows when that date is removed as the calculation will 
# be from the next reserved date.
# Should this be kept in a special file/table of bumped shows ?
#

for (my $count = 1; $count <= 200; $count++)
{
  $clock= $clock+86400; # Go to the next day
  my $this_date = time2str("%Y-%m-%d", $clock);
  my $day_of_week = time2str("%A", $clock);
  my $day_of_month = int(time2str("%d", $clock));
  my $this_week_number = time2str("%U", $clock);
#  if ( ( $day_of_week eq "Saturday" ) || $day_of_week eq "Sunday" ) { next; }
#  print "--- --- --- --- --- --- --- --- --- --- $this_week_number $this_date -> 2012-09-21\n";
  # No shows on Saturday and Sunday
  if ( $day_of_week eq "Saturday" ) { next; }
  if ( $day_of_week eq "Sunday" ) { 
    print "--- --- --- --- --- --- --- --- --- --- \n";
    next; 
  }
  # 1st Monday every month: HPR Admins with "Community News"
  if ( ( $day_of_week eq "Monday" ) && ( $day_of_month <= 7 ) ) { 
    print "$this_date\tHPR Community News (RESERVED SLOT)\n";
    next;
  }
  # Every second Tuesday: linux in the shell
  if ( ( $day_of_week  eq "Tuesday" ) && ( $this_week_number%2 == 0 ) ) {
    print "$this_date\tLinux in the Shell (RESERVED SLOT)\n";
    next;
  }
  if ( ( $day_of_week eq "Thursday" ) && ( $day_of_month <= 7 ) ) { 
    print "$this_date\tSunday Morning Linux Review (RESERVED SLOT)\n";
    next;
  }
  if ( ( $day_of_week eq "Thursday" ) && ( $day_of_month > 7 ) ) { 
    print "$this_date\t---> FREE SLOT (Syndicated Thursday)\n";
    next;
  }
  if ( ( $day_of_week eq "Friday" ) && ( $day_of_month <= 21 ) ) { 
    print "$this_date\tDeepGeek TGTM Newscast (RESERVED SLOT)\n";
    next;
  }
  print "$this_date\t---> FREE SLOT\n";
}

