#!/usr/bin/perl
# UTF-8 Test >> ÇirçösÚáóíéőöÓÁśł <<

use strict;
use warnings;
use utf8;
use Date::Format;
use Date::Calc qw( Mktime );
use Data::Dumper::Simple;
binmode STDOUT, ":utf8";

my ( @queue, %specialdate, %specialnum ) = ();
my $file = "";

# --------------------------------------------------------------------------
# Temp variables to be automatically defined later
my $lastshow = '975';
my $maxshow = $lastshow + 500;

# --------------------------------------------------------------------------
# Read the shows into a @shows array
$file = "shows.txt";
open(FILE, "<:utf8", $file) or die "Couldn't open show queue file: $!";
my @shows = <FILE>;
close (FILE);
chomp (@shows);

# --------------------------------------------------------------------------
# Read the syndicated thursday shows into a @syndicated array
$file = "syndicated-thursdays.txt";
open(FILE, "<:utf8", $file) or die "Couldn't open syndicated thursday queue file: $!";
my @syndicated = <FILE>;
close (FILE);
chomp (@syndicated);

# --------------------------------------------------------------------------
# Read the reserved special dates into a %specialdate hash
$file = "special-date.txt";
open(FILE, "<:utf8", $file) or die "Couldn't open special date file: $!";
while (<FILE>) {
  next if ( /^\s*#/ ); # skip comments
  chomp;
  if ( ! /^20\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])\t/ ){
    #print STDERR "$file contains an invalid date in the line: \"$_\"\n"; 
    next;
  }
  my ($key, $value) = split (/\t/, $_);
  $specialdate{$key} = $value; 
}
close (FILE);
# foreach (sort keys(%specialdate) ) { print "key: $_ value: $specialdate{$_}\n" }

# --------------------------------------------------------------------------
# Read the reserved special dates into a %specialnum hash, 
$file = "special-num.txt";
open(FILE, "<:utf8", $file) or die "Couldn't open special episode number file: $!";
while (<FILE>) {
  next if ( /^\s*#/ ); # skip comments
  chomp;
  if ( ! /^\d{3,4}\t/ ){
    #print STDERR "$file contains an invalid episode number in the line: \"$_\"\n"; 
    next;
  }
  my ($key, $value) = split (/\t/, $_);
  $specialnum{$key} = $value; 
}
#foreach (sort {$a<=>$b} keys(%specialnum) ) { print "key: $_ value: $specialnum{$_}\n" }

# --------------------------------------------------------------------------
# Make a list of dates that are reserved.
my $clock= 86400 + Mktime( time2str("%Y", time), time2str("%m", time), time2str("%d", time), 0, 0, 0);
# The loop should begin from the first reserved date so we catch items in the past,
# however we need to keep track of bumped down shows when that date is removed as the calculation will 
# be from the next reserved date.
# Should this be kept in a special file/table of bumped shows ?
#

for (my $count = 1; $count <= 200; $count++)
{
  $clock= $clock+86400; # Go to the next day
  my $this_date = time2str("%Y-%m-%d", $clock);
  my $day_of_week = time2str("%A", $clock);
  my $day_of_month = int(time2str("%d", $clock));
  my $this_week_number = time2str("%U", $clock);

  # Check if it's a special day. Reserved slots will get bumped down
  if ( ( exists $specialdate{ $this_date } ) && ( defined $specialdate{ $this_date } ) ) {
    push ( @queue, $this_date . "\t" . $specialdate{ $this_date } );
  }
  # No shows on Saturday and Sunday
  if ( ( $day_of_week eq "Saturday" ) || $day_of_week eq "Sunday" ) { next; }
  # 1st Monday every month: HPR Admins with "Community News"
  if ( ( $day_of_week eq "Monday" ) && ( $day_of_month <= 7 ) ) { 
    push ( @queue, $this_date . "\tHPR Community News (RESERVED SLOT)" );
  }
  # Every second Tuesday: linux in the shell
  if ( ( $day_of_week  eq "Tuesday" ) && ( $this_week_number%2 == 0 ) ) {
    push ( @queue, $this_date . "\tLinux in the Shell (RESERVED SLOT)" );
  }
  if ( ( $day_of_week eq "Thursday" ) && ( $day_of_month <= 7 ) ) { 
    push ( @queue, $this_date . "\tSunday Morning Linux Review (RESERVED SLOT)" );
  }
  if ( ( $day_of_week eq "Friday" ) && ( $day_of_month <= 21 ) ) { 
    push ( @queue, $this_date . "\tDeepGeek TGTM Newscast (RESERVED SLOT)" );
  }
}

print Dumper ( @queue );