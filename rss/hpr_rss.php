<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" >
<channel>
  <title>Hacker Public Radio</title>
  <link>http://hackerpublicradio.org/about.php</link>
  <itunes:subtitle>A daily show hosted the community on topics that are of interest to hackers and hobbyists.</itunes:subtitle>
  <description>Hacker Public Radio is an podcast that releases shows every weekday Monday through Friday. Our shows are produced by the community (you) and can be on any topic that is "are of interest to hackers and hobbyists".</description>
  <language>en-us</language>
  <itunes:category text="Technology">
    <itunes:category text="Tech News"/>
  </itunes:category>
  <itunes:category text="Education">
    <itunes:category text="Training"/>
  </itunes:category>
  <itunes:image href="http://hackerpublicradio.org/images/hpr_feed.png"/>
  <itunes:explicit>yes</itunes:explicit>
  <itunes:keywords>Community Radio, Tech Interviews, Linux, Open, Hobby, Software Freedom</itunes:keywords>
  <copyright>Creative Commons Attribution-ShareAlike 3.0 License</copyright>
  <managingEditor>feedback@NOSPAM-hackerpublicradio.org (HPR Feedback)</managingEditor> 
  <itunes:owner>    
    <itunes:name>HPR Webmaster</itunes:name>
    <itunes:email>admin@hackerpublicradio.org</itunes:email>
  </itunes:owner>
  <webMaster>admin@NOSPAM-hackerpublicradio.org (HPR Webmaster)</webMaster> 
  <generator>kate</generator> 
  <docs>http://www.rssboard.org/rss-specification</docs>
  <ttl>3200</ttl>
  <skipDays>
    <day>Saturday</day>
    <day>Sunday</day>
  </skipDays>
  <image>
    <url>http://hackerpublicradio.org/images/hpr_feed_small.png</url>
    <title>Hacker Public Radio</title>
    <link>http://hackerpublicradio.org/about.php</link>
    <description>The Hacker Public Radio Old Microphone Logo</description>
    <height>96</height>
    <width>120</width>
  </image>
  <atom:link href="http://hackerpublicradio.org/hpr_mp3_rss.php" rel="self" type="application/rss+xml" />
  <pubDate>Thu, 23 Feb 2012 00:00:00 +0000</pubDate>
  <item>
    <title>HPR0928: My Linux Adventure, Pt. 1</title>
    <author>rbrtewdn.nospam@nospam.comcast.net (Bob Wooden)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=206</link>
    <description><![CDATA[
Release year - 2012
Contact Info: bob.wooden@comcast.net


Links mentioned: 


Redhat
http://www.redhat.com
http://fedoraproject.org


SuSE
http://www.suse.com
http://www.opensuse.org


Micro Center - (my opinion - great retail environment for computer parts)
http://www.microcenter.com/
http://en.wikipedia.org/wiki/Micro_Center


The properitory software device that does not allow printing or saving information without &quot;key&quot; was called &quot;Design Key&quot; or &quot;Software Dongle&quot; by myself. This is the brand &quot;we used&quot; (were provided) by design software (CAD type) kitchen and bathroom design program. (This is the &quot;dongle&quot; device. I do not care if I mention the properatory software name. It's not very good and it's . . . well, properatory.)
http://www.petrotechnics.com/sentinel.html


Linux Terminal Server Project (LTSP)
http://www.ltsp.org/
http://en.wikipedia.org/wiki/Linux_Terminal_Server_Project


Network File System (NFS)
http://en.wikipedia.org/wiki/Network_File_System


Dynamic Host Configuration Protocol (DHCP)
http://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol
]]>
</description>
    <itunes:summary><![CDATA[
Release year - 2012
Contact Info: bob.wooden@comcast.net


Links mentioned: 


Redhat
http://www.redhat.com
http://fedoraproject.org


SuSE
http://www.suse.com
http://www.opensuse.org


Micro Center - (my opinion - great retail environment for computer parts)
http://www.microcenter.com/
http://en.wikipedia.org/wiki/Micro_Center


The properitory software device that does not allow printing or saving information without &quot;key&quot; was called &quot;Design Key&quot; or &quot;Software Dongle&quot; by myself. This is the brand &quot;we used&quot; (were provided) by design software (CAD type) kitchen and bathroom design program. (This is the &quot;dongle&quot; device. I do not care if I mention the properatory software name. It's not very good and it's . . . well, properatory.)
http://www.petrotechnics.com/sentinel.html


Linux Terminal Server Project (LTSP)
http://www.ltsp.org/
http://en.wikipedia.org/wiki/Linux_Terminal_Server_Project


Network File System (NFS)
http://en.wikipedia.org/wiki/Network_File_System


Dynamic Host Configuration Protocol (DHCP)
http://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol
]]>
</itunes:summary>
    <pubDate>Tue, 21 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0928.mp3" length="13047276" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0928.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0927: Setting up a WordPress blog: part 1</title>
    <author>frankwbell.nospam@nospam.cox.net (Frank Bell)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=195</link>
    <description><![CDATA[
Frank Bell summarizes the steps involved in setting up a WordPress blog.  This episode covers creating a database and database user, installing the WordPress software, and configuring basic WordPress settings.


Related links:

WordPress Software, including the codex, themes, and plugins. (http://wordpress.org/)
Wordpress blog hosting site (http://wordpress.com/)
Xampp LAMPP server stack. (http://www.apachefriends.org/en/xampp.html)
MySQL (http://mysql.com/)

Some other blog hosting sites:  
Blogger (http://blogger.com)
Blogspot (http://blogspot.com)
Typepad (http://www.typepad.com/)
Tumblr ()]]>
</description>
    <itunes:summary><![CDATA[
Frank Bell summarizes the steps involved in setting up a WordPress blog.  This episode covers creating a database and database user, installing the WordPress software, and configuring basic WordPress settings.


Related links:

WordPress Software, including the codex, themes, and plugins. (http://wordpress.org/)
Wordpress blog hosting site (http://wordpress.com/)
Xampp LAMPP server stack. (http://www.apachefriends.org/en/xampp.html)
MySQL (http://mysql.com/)

Some other blog hosting sites:  
Blogger (http://blogger.com)
Blogspot (http://blogspot.com)
Typepad (http://www.typepad.com/)
Tumblr ()]]>
</itunes:summary>
    <pubDate>Tue, 21 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0927.mp3" length="22728096" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0927.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0926: Heresies in the year of the apocalypse ep 1 - computer languages</title>
    <author>hpr.nospam@nospam.mrgadgets.com (MrGadgets)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=155</link>
    <description><![CDATA[
Mr Gadgests calls in Apokoplictic year 2012 where he discusses Assembler, cobal and Grace Hopper

From: http://en.wikipedia.org/wiki/Grace_Hopper

Rear Admiral Grace Murray Hopper (December 9, 1906 – January 1, 1992) was an American computer scientist and United States Navy officer. A pioneer in the field, she was one of the first programmers of the Harvard Mark I computer, and developed the first compiler for a computer programming language. She conceptualized the idea of machine-independent programming languages, which led to the development of COBOL, one of the first modern programming languages. She is credited with popularizing the term &quot;debugging&quot; for fixing computer glitches (motivated by an actual moth removed from the computer). Because of the breadth of her accomplishments and her naval rank, she is sometimes referred to as &quot;Amazing Grace.&quot; The U.S. Navy destroyer USS Hopper (DDG-70) was named for her, as was the Cray XE6 &quot;Hopper&quot; supercomputer at NERSC.
]]>
</description>
    <itunes:summary><![CDATA[
Mr Gadgests calls in Apokoplictic year 2012 where he discusses Assembler, cobal and Grace Hopper

From: http://en.wikipedia.org/wiki/Grace_Hopper

Rear Admiral Grace Murray Hopper (December 9, 1906 – January 1, 1992) was an American computer scientist and United States Navy officer. A pioneer in the field, she was one of the first programmers of the Harvard Mark I computer, and developed the first compiler for a computer programming language. She conceptualized the idea of machine-independent programming languages, which led to the development of COBOL, one of the first modern programming languages. She is credited with popularizing the term &quot;debugging&quot; for fixing computer glitches (motivated by an actual moth removed from the computer). Because of the breadth of her accomplishments and her naval rank, she is sometimes referred to as &quot;Amazing Grace.&quot; The U.S. Navy destroyer USS Hopper (DDG-70) was named for her, as was the Cray XE6 &quot;Hopper&quot; supercomputer at NERSC.
]]>
</itunes:summary>
    <pubDate>Mon, 20 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0926.mp3" length="23015259" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0926.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0925: TGTM Tech News for 2012-02-15</title>
    <author>hpr.nospam@nospam.deepgeek.us  (deepgeek)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=73</link>
    <description><![CDATA[TGTM Tech News for 2012-02-15

Shownotes are taken from Show Notes for TGTM news 60


Here is a news review:

  One Year Later (case of Florida cop Jimmy Dac Ho murdering a sex worker.)
  U.S., Banks Reach $26 Billion Settlement for Mortgage Abuses  
  Mitt Romney: Relish the Rich, Ignore the Poor  
  Beware Of Those Who Claim They're 'Saving The Culture Business' When They're Really Protecting Those Who Strip Artists Of Rights  
  The Wrong Path on Cuba  
  India’s Downward Spiral  
  Letters to the Copyright Office: Why I Jailbreak  
  BitTorrent Giant BTjunkie Shuts Down For Good  
  Congress Actually Helping The Internet, Rather Than Mucking It Up?  
  Download a Copy of The Pirate Bay, It’s Only 90 MB
  

Other Headlines:

  The Haditha Massacre: No Justice for Iraqis  
  How Swedes and Norwegians Broke the Power of the &quot;One Percent&quot;  
  WikiLeaks suspect Bradley Manning arraignment for February 23 
  Army Private Manning nominated for Nobel Peace Prize for role in leaking documents 
  Roseanne Barr's Candidacy for President of America Via the Green Party
  

News from &quot;havanatimes.org, &quot; &quot;maggiemcneill.wordpress.com, &quot;techdirt.com,&quot; and &quot;ufcw.blogspot.com&quot; used under arranged permission. News from &quot;eff.org&quot;  and &quot;torrentfreak.com&quot; used under permission of the Creative Commons by-attribution license. News from &quot;democracynow.org&quot; used under permission of the Creative Commons by-attribution non-commercial no-derivatives license.
News Sources retain their respective copyrights.

Links
http://www.talkgeektome.us/tgtmnews-60.html
http://maggiemcneill.wordpress.com/2012/02/09/one-year-later/
http://www.democracynow.org/2012/2/9/headlines#0
http://ufcw.blogspot.com/2012/02/mitt-romney-relish-rich-ignore-poor.html
http://www.techdirt.com/articles/20120201/00433217612/beware-those-who-claim-theyre-saving-culture-business-when-theyre-really-protecting-those-who-strip-artists-rights.shtml
http://www.havanatimes.org/?p=61712
https://www.eff.org/deeplinks/2012/02/india%E2%80%99s-downward-spiral
https://www.eff.org/deeplinks/2012/02/letters-copyright-office-why-i-jailbreak
http://torrentfreak.com/btjunkie-shuts-down-for-good-120206/
http://www.techdirt.com/articles/20120208/00260717693/congress-actually-helping-internet-rather-than-mucking-it-up.shtml
http://torrentfreak.com/download-a-copy-of-the-pirate-bay-its-only-90-mb-120209/
http://sacsis.org.za/site/article/1191
http://www.truth-out.org/how-swedes-and-norwegians-broke-power-one-percent/1327942221
http://www.rawstory.com/rs/2012/02/09/wikileaks-suspect-bradley-manning-arraignment-for-february-23/
http://www.krmg.com/news/news/local/army-private-manning-nominated-nobel-peace-prize-r/nHYNR/
http://sacsis.org.za/site/article/1201
]]>
</description>
    <itunes:summary><![CDATA[TGTM Tech News for 2012-02-15

Shownotes are taken from Show Notes for TGTM news 60


Here is a news review:

  One Year Later (case of Florida cop Jimmy Dac Ho murdering a sex worker.)
  U.S., Banks Reach $26 Billion Settlement for Mortgage Abuses  
  Mitt Romney: Relish the Rich, Ignore the Poor  
  Beware Of Those Who Claim They're 'Saving The Culture Business' When They're Really Protecting Those Who Strip Artists Of Rights  
  The Wrong Path on Cuba  
  India’s Downward Spiral  
  Letters to the Copyright Office: Why I Jailbreak  
  BitTorrent Giant BTjunkie Shuts Down For Good  
  Congress Actually Helping The Internet, Rather Than Mucking It Up?  
  Download a Copy of The Pirate Bay, It’s Only 90 MB
  

Other Headlines:

  The Haditha Massacre: No Justice for Iraqis  
  How Swedes and Norwegians Broke the Power of the &quot;One Percent&quot;  
  WikiLeaks suspect Bradley Manning arraignment for February 23 
  Army Private Manning nominated for Nobel Peace Prize for role in leaking documents 
  Roseanne Barr's Candidacy for President of America Via the Green Party
  

News from &quot;havanatimes.org, &quot; &quot;maggiemcneill.wordpress.com, &quot;techdirt.com,&quot; and &quot;ufcw.blogspot.com&quot; used under arranged permission. News from &quot;eff.org&quot;  and &quot;torrentfreak.com&quot; used under permission of the Creative Commons by-attribution license. News from &quot;democracynow.org&quot; used under permission of the Creative Commons by-attribution non-commercial no-derivatives license.
News Sources retain their respective copyrights.

Links
http://www.talkgeektome.us/tgtmnews-60.html
http://maggiemcneill.wordpress.com/2012/02/09/one-year-later/
http://www.democracynow.org/2012/2/9/headlines#0
http://ufcw.blogspot.com/2012/02/mitt-romney-relish-rich-ignore-poor.html
http://www.techdirt.com/articles/20120201/00433217612/beware-those-who-claim-theyre-]]>
</itunes:summary>
    <pubDate>Thu, 16 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0925.mp3" length="11251192" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0925.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0924: Episode 000 redirection</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (Dann)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=7</link>
    <description><![CDATA[Welcome to the first entry of Linux in the Shell.  Before delving into specific commands, redirection will be explored as redirection will be used frequently in the examples going forward.  The Unix philosophy posits program simplicity and that a program should do one thing and do it well (Mike Gancarz, the Unix Philosophy).  Eric Raymond adds the Rule of Composition:  &quot;Design programs to be connected to other programs.&quot;  Redirection is the glue that achieves this design.

Redirection is applied to any of the following standard streams to achieve results beyond simply outputting some value from a single command:

Standard Input (stdin) – 0
Standard Output (stdout) – 1
Standard Error (stderr) – 2

For the rest of this article and accompanying video please go to http://www.linuxintheshell.org/2012/02/16/entry-000-redirection/
The video can be downloaded http://www.archive.org/download/LinuxInTheShellEpisode000-Redirection/lits-000.ogv]]>
</description>
    <itunes:summary><![CDATA[Welcome to the first entry of Linux in the Shell.  Before delving into specific commands, redirection will be explored as redirection will be used frequently in the examples going forward.  The Unix philosophy posits program simplicity and that a program should do one thing and do it well (Mike Gancarz, the Unix Philosophy).  Eric Raymond adds the Rule of Composition:  &quot;Design programs to be connected to other programs.&quot;  Redirection is the glue that achieves this design.

Redirection is applied to any of the following standard streams to achieve results beyond simply outputting some value from a single command:

Standard Input (stdin) – 0
Standard Output (stdout) – 1
Standard Error (stderr) – 2

For the rest of this article and accompanying video please go to http://www.linuxintheshell.org/2012/02/16/entry-000-redirection/
The video can be downloaded http://www.archive.org/download/LinuxInTheShellEpisode000-Redirection/lits-000.ogv]]>
</itunes:summary>
    <pubDate>Wed, 15 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0924.mp3" length="12055732" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0924.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0923: 12 Gazillion Buttons</title>
    <author>nybill.nospam@nospam.gunmonkeynet.net (Jezra and NYbill)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=205</link>
    <description><![CDATA[
Jezra and NYbill discuss their predictions for 2012 and the things they are looking forward to in the new
year.  The discussion moves on to LUG's.  Jezra takes Bill on a trip down memory lane. Then Bill strikes a nerve with Jez who rants about 3D movies (Language warning).  They finish up talking about their
current hardware and software projects.

Links

http://www.jezra.net/blog Jezra
http://identi.ca/nybill NYbill
http://www.raspberrypi.org/ Raspberry Pi
http://www.pjrc.com/teensy/ Teensy
http://www.northeastlinuxfest.org/ Northeast Linux Fest
http://www.socallinuxexpo.org/scale10x The Southern California Linux Expo
http://nblug.org/ The North Bay Linux Users Group
http://cdlug.net/ Capital District Linux Users Group
http://http://www.2600.com/ 2600
http://2600albany.wordpress.com/ Albany2600

]]>
</description>
    <itunes:summary><![CDATA[
Jezra and NYbill discuss their predictions for 2012 and the things they are looking forward to in the new
year.  The discussion moves on to LUG's.  Jezra takes Bill on a trip down memory lane. Then Bill strikes a nerve with Jez who rants about 3D movies (Language warning).  They finish up talking about their
current hardware and software projects.

Links

http://www.jezra.net/blog Jezra
http://identi.ca/nybill NYbill
http://www.raspberrypi.org/ Raspberry Pi
http://www.pjrc.com/teensy/ Teensy
http://www.northeastlinuxfest.org/ Northeast Linux Fest
http://www.socallinuxexpo.org/scale10x The Southern California Linux Expo
http://nblug.org/ The North Bay Linux Users Group
http://cdlug.net/ Capital District Linux Users Group
http://http://www.2600.com/ 2600
http://2600albany.wordpress.com/ Albany2600

]]>
</itunes:summary>
    <pubDate>Tue, 14 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0923.mp3" length="17076801" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0923.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0922: Updating a Garmin GPS for free</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (riddlebox)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=94</link>
    <description><![CDATA[
Notice
**I am not condoning this method I am just reporting that I have read
on numerous sites the steps and procedures on how to do this.****
Sources:



http://www.haklabs.com/2011/garmin-2012-map-update/

http://www.youtube.com/watch?v=DBlS0fzWjbY

http://thepiratebay.org/torrent/6536173/Garmin_City_Navigator_North_America_NT_2012.10_IMG

Updating a Garmin GPS for free

This is a guide that I have compiled on how to upgrade a Garmin GPS to the 2012 maps for free. First you must torrent the &quot;Garmin City Navigator 2012.10 torrent file,  or the 2012.20 torrent file.

2012.10
http://www.megaupload.com/?d=9DAC19AT

2012.20
http://www.multiupload.com/L4AVMTU4WA

Then you extract the contents of that into a folder and 





Connect your device to the computer.

Go into the Nuvi files and backup the file named gmapprom.img to your computer.

Delete the gmapprom.img file from the device. (note: make sure you empty the recycle bin after this step)

Delete any unused Language files too..

Copy the unlocked gmapprom.img file that you downloaded into the device. If the downloaded file is named something else, rename it to gmapprom.img and then put it on your devices internal memory.

Restart your device and check your map info via : Tools&amp;gt;Settings&amp;gt;Map&amp;gt;Map Info.

There you have it! You have done it.

]]>
</description>
    <itunes:summary><![CDATA[
Notice
**I am not condoning this method I am just reporting that I have read
on numerous sites the steps and procedures on how to do this.****
Sources:



http://www.haklabs.com/2011/garmin-2012-map-update/

http://www.youtube.com/watch?v=DBlS0fzWjbY

http://thepiratebay.org/torrent/6536173/Garmin_City_Navigator_North_America_NT_2012.10_IMG

Updating a Garmin GPS for free

This is a guide that I have compiled on how to upgrade a Garmin GPS to the 2012 maps for free. First you must torrent the &quot;Garmin City Navigator 2012.10 torrent file,  or the 2012.20 torrent file.

2012.10
http://www.megaupload.com/?d=9DAC19AT

2012.20
http://www.multiupload.com/L4AVMTU4WA

Then you extract the contents of that into a folder and 





Connect your device to the computer.

Go into the Nuvi files and backup the file named gmapprom.img to your computer.

Delete the gmapprom.img file from the device. (note: make sure you empty the recycle bin after this step)

Delete any unused Language files too..

Copy the unlocked gmapprom.img file that you downloaded into the device. If the downloaded file is named something else, rename it to gmapprom.img and then put it on your devices internal memory.

Restart your device and check your map info via : Tools&amp;gt;Settings&amp;gt;Map&amp;gt;Map Info.

There you have it! You have done it.

]]>
</itunes:summary>
    <pubDate>Mon, 13 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0922.mp3" length="4678323" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0922.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0921: Tag Team Chase Douglas Interview with Alison Chaiken</title>
    <author>marcoz.nospam@nospam.osource.org (marcoz)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=156</link>
    <description><![CDATA[Chase Douglas is a software developer at Canonical working primarily on multitouch user interface support. For the past year, Chase has been involved with developing gesture support through Canonical’s uTouch framework and multitouch support through the X.org window system. Prior to working on multitouch, Chase spent three years performing Linux kernel and plumbing layer development and maintenance at Canonical and IBM.

Alison's questions:
3:49 -  Alison asks &quot;Chase, back up for a  moment, can you talk a little bit about what X input is and how X in general works in Linux.&quot;

6:13 - Alison asks &quot;Do you have any particular target hardware that you are thinking about during its development?&quot;

11:57 - Alison asks &quot;Do we expect the mouse and keyboard to be with us in the long term? Are you really thinking of all these touches used in concert with the mouse and keyboard or that we may be evolving away from that?&quot;

17:45 - Alison basically asks &quot;Is there talk about an agreed upon gesture language?&quot;

20:56 - Alison asks &quot;What is the state of device driver support for capacitive screens that will support multitouch in Linux?&quot;

26:34 - Alison asks &quot;Speaking of software coupling, are you looking at Wayland already or is that still over the horizon?&quot;

28:43 - Alison says &quot;The automotive case seems like a fascinating one. As far as touch and gesture goes and Ubuntu has an IDI and recently Cadillac has a multitouch screen that has haptic feedback and some gesture support.  This looks like a very exciting area for development. Actual shipping products in 2012.  I don't know if you're familiar with that at all.&quot;

32:11 - Alison asks &quot;Do you anticipate contributing the multitouch work to GNOME and Debian as well?

35:0 - Alin asks &quot;What new features can we anticipate that will be user visible for precision in the area of multitouch and gestures?&quot;

43:56 - Alison says &quot;I think I'm happy although I must mention I was pained to hear that it was 24 years ago that you were an infant because I was at M.I.T when they started the X project.  heh heh.  you young whippersnappers.
...
that was very fascinating.  I had no idea there was that much activity going on. I'm really excited to see what's coming out and what new features are being added.&quot;

launchpad.net/utouch
multi-touch-dev@lists.launchpad.net

#ubuntu-touch   irc.freenode.net]]>
</description>
    <itunes:summary><![CDATA[Chase Douglas is a software developer at Canonical working primarily on multitouch user interface support. For the past year, Chase has been involved with developing gesture support through Canonical’s uTouch framework and multitouch support through the X.org window system. Prior to working on multitouch, Chase spent three years performing Linux kernel and plumbing layer development and maintenance at Canonical and IBM.

Alison's questions:
3:49 -  Alison asks &quot;Chase, back up for a  moment, can you talk a little bit about what X input is and how X in general works in Linux.&quot;

6:13 - Alison asks &quot;Do you have any particular target hardware that you are thinking about during its development?&quot;

11:57 - Alison asks &quot;Do we expect the mouse and keyboard to be with us in the long term? Are you really thinking of all these touches used in concert with the mouse and keyboard or that we may be evolving away from that?&quot;

17:45 - Alison basically asks &quot;Is there talk about an agreed upon gesture language?&quot;

20:56 - Alison asks &quot;What is the state of device driver support for capacitive screens that will support multitouch in Linux?&quot;

26:34 - Alison asks &quot;Speaking of software coupling, are you looking at Wayland already or is that still over the horizon?&quot;

28:43 - Alison says &quot;The automotive case seems like a fascinating one. As far as touch and gesture goes and Ubuntu has an IDI and recently Cadillac has a multitouch screen that has haptic feedback and some gesture support.  This looks like a very exciting area for development. Actual shipping products in 2012.  I don't know if you're familiar with that at all.&quot;

32:11 - Alison asks &quot;Do you anticipate contributing the multitouch work to GNOME and Debian as well?

35:0 - Alin asks &quot;What new features can we anticipate that will be user visible for precision in the area of multitouch and gestures?&quot;

43:56 - Alison says &quot;I think I'm happy although I must mention I was pained to hear that it was 24 years ago that you were an infant because I was at M.I.T when they started the X project.  heh heh.  you young whippersnappers.
...
that was very fascinating.  I had no idea there was that much activity going on. I'm really excited to see what's coming out and what new features are being added.&quot;

launchpad.net/utouch
multi-touch-dev@lists.launchpad.net

#ubuntu-touch   irc.freenode.net]]>
</itunes:summary>
    <pubDate>Sun, 12 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0921.mp3" length="32878594" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0921.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0920: TGTM Newscast for 2012/02/08</title>
    <author>hpr.nospam@nospam.deepgeek.us  (deepgeek)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=73</link>
    <description><![CDATA[TGTM Newscast for 2012/02/08 DeepGeek
We have had alot happen in the world since the last newscast, so
let's jump right in! We're also rich in audio interludes, so I will be
inserting them almost between the non-tech news stories.

Here is a news review:

  Calls for Julian Assange to be tried as terrorist under NDAA-like law in 2010
  This story is important because there was a movement to have multiple political parties in Cuba. Cuba Keeps One Party, Sets Term Limits 
  
Let's start a segment of three stories regarding United States of America politics. First, a story about the struggle against the &quot;Electoral College&quot; system of voting, which is a key method this country uses to suppress anything besides to two dominant fascist (in the technical sense of being a marriage between corporations and government) parties. We will see how the state of Washington is fighting that system. After this, a story about the fight for decent medical care for the country within the state of California. Lastly, an editorial written by one of the co-chairs of the Socialist Party USA about Ron Paul.

It should be noted that I am not yet &quot;endorsing&quot; the  Socialist Party USA, merely covering their point of view which the corporate media ignores in order to keep them out of the minds of the people of the USA. That's the point of TGTM news, to report the stories they suppress. The Green Party USA is very interesting also.
Jill Stein is fighting for candidacy for President with them, and she recently issued her own &quot;Peoples State of the Union (which I will link to in the &quot;other headlines&quot; section)&quot; right after Obama's State of the Union. While Obama's State Of The Union was very Ronald Regan, you know, with it's tough-guy statements about forcing other countries to respect us via Military Force and it's &quot;trickle down economics&quot; statements about boosting the economy by giving even more handouts to mega-corporations; Jill Stein's message was about &quot;the Green New Deal,&quot; about stimulating the economy with direct-to-locality  stimulus. As matter of fact, she held a video chat direct to the people who got to ask her questions via chat room. Remembering that a union I regularly cover, the IWW, encourages it's members to work via worker co-operatives, I asked if co-ops would be locked out of the &quot;Green New Deal.&quot; Ms. Stein answered my question by stating that co-operatives were valid recipients of contracts, and that the point was that, historically, big corporations pocket too much for their owners, so she only wanted to lock out the Interstate and large corporations to aid small business units.
Jill Stein is currently running against Roseanne Barr for the candidacy for presidency in the Green Party USA.

Court Approves Washington State System of Limiting November Ballot Access to Two Candidates

  California's single-payer health bill moves forward
  The Misadventure of Ron Paul
  ACLU &amp;amp; EFF to Appeal Secrecy Ruling in Twitter/WikiLeaks CaseEditorial Comment: After the ruling the Icelandic Member-of-Parliment, Ms. Birgitta Jonsdottir, &quot;broke silence&quot; on the matter of this case. So I included, in the &quot;other headlines&quot; section, links to her blog entry about it, as well as a link to a Radio Netherlands International english podcast that includes an interview with her.
  The Right to Anonymity is a Matter of Privacy
  MegaUpload: What Made It a Rogue Site Worthy of Destruction?
  Mega Aftermath: Upheaval In Pirate Warez Land
  New Venezuelan Social Network Takes Off

Other Headlines:

  People's State of the Union: A Green New Deal for America
  Supreme Court rules Congress can re-copyright public domain works
  MY TWITTER CASE AND &quot;THOUGHTCRIME&quot; 
  The State We're In - Freedom's Road
  27 of 35 Bush Articles of Impeachment Apply to Obama

News from &quot;havanatimes.org, &quot; &quot;allgov.com,&quot; and &quot;dissidentvoice.org&quot; &quot;used under arranged permission. News from &quot;eff.org&quot;  and &quot;torrentfreak.com&quot; used
under permission of the Creative Commons by-attribution license. News from &quot;wlcentral.org&quot; and &quot;peoplesworld.org&quot; used under permission of the Creative
Commons by-attribution non-commercial no-derivatives license. News from &quot;venezuelanalysis.com&quot; is copyleft.

Audio Interlude, MOC #112, used under permission of Lee Camp.

News Sources retain their respective copyrights.
Links

http://www.havanatimes.org/?p=60933

http://www.allgov.com//ViewNews/Court_Approves_Washington_State_System_of_Limiting_November_Ballot_Access_to_Two_Candidates_120122

http://peoplesworld.org/california-s-single-payer-health-bill-moves-forward/

http://dissidentvoice.org/2012/01/the-misadventure-of-ron-paul/

https://www.eff.org/press/releases/aclu-eff-appeal-secrecy-ruling-twitterwikileaks-case

https://www.eff.org/deeplinks/2012/01/right-anonymity-matter-privacy

http://torrentfreak.com/megaupload-what-made-it-a-rogue-site-worthy-of-destruction-120120/

http://torrentfreak.com/mega-aftermath-upheaval-in-pirate-warez-land-120128/

http://venezuelanalysis.com/news/6772

http://www.web.gpnys.com/?p=11479

http://arstechnica.com/tech-policy/news/2012/01/supreme-court-rules-congress-can-re-copyright-public-domain-works.ars

http://joyb.blogspot.com/2011/03/my-twitter-case-and-thoughtcrime.html

http://www.rnw.nl/english/radioshow/freedoms-road

http://bigbatusa.org/david-swanson-27-of-35-bush-articles-of-impeachment-apply-to-obama/


]]>
</description>
    <itunes:summary><![CDATA[TGTM Newscast for 2012/02/08 DeepGeek
We have had alot happen in the world since the last newscast, so
let's jump right in! We're also rich in audio interludes, so I will be
inserting them almost between the non-tech news stories.

Here is a news review:

  Calls for Julian Assange to be tried as terrorist under NDAA-like law in 2010
  This story is important because there was a movement to have multiple political parties in Cuba. Cuba Keeps One Party, Sets Term Limits 
  
Let's start a segment of three stories regarding United States of America politics. First, a story about the struggle against the &quot;Electoral College&quot; system of voting, which is a key method this country uses to suppress anything besides to two dominant fascist (in the technical sense of being a marriage between corporations and government) parties. We will see how the state of Washington is fighting that system. After this, a story about the fight for decent medical care for the country within the state of California. Lastly, an editorial written by one of the co-chairs of the Socialist Party USA about Ron Paul.

It should be noted that I am not yet &quot;endorsing&quot; the  Socialist Party USA, merely covering their point of view which the corporate media ignores in order to keep them out of the minds of the people of the USA. That's the point of TGTM news, to report the stories they suppress. The Green Party USA is very interesting also.
Jill Stein is fighting for candidacy for President with them, and she recently issued her own &quot;Peoples State of the Union (which I will link to in the &quot;other headlines&quot; section)&quot; right after Obama's State of the Union. While Obama's State Of The Union was very Ronald Regan, you know, with it's tough-guy statements about forcing other countries to respect us via Military Force and it's &quot;trickle down economics&quot; statements about boosting the economy by giving even more handouts to mega-corporations; Jill Stein's message was about &quot;the Green New Deal,&quot; about stimulating the economy with direct-to-locality  stimulus. As matter of fact, she held a video chat direct to the people who got to ask her questions via chat room. Remembering that a union I regularly cover, the IWW, encourages it's members to work via worker co-operatives, I asked if co-ops would be locked out of the &quot;Green New Deal.&quot; Ms. Stein answered my question by stating that co-operatives were valid recipients of contracts, and that the point was that, historically, big corporations pocket too much for their owners, so she only wanted to lock out the Interstate and large corporations to aid small business units.
Jill Stein is currently running against Roseanne Barr for the candidacy for presidency in the Green Party USA.

Court Approves Washington State System of Limiting November Ballot Access to Two Candidates

  California's single-payer health bill moves forward
  The Misadventure of Ron Paul
  ACLU &amp;amp; EFF to Appeal Secrecy Ruling in Twitter/WikiLeaks CaseEditorial Comment: After the ruling the Icelandic Member-of-Parliment, Ms. Birgitta Jonsdottir, &quot;broke silence&quot; on the matter of this case. So I included, in the &quot;other headlines&quot; section, links to her blog entry about it, as well as a link to a Radio Netherlands International english podcast that includes an interview with her.
  ]]>
</itunes:summary>
    <pubDate>Thu, 09 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0920.mp3" length="15756959" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0920.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0919: Elfstedentocht - To be or not to be</title>
    <author>ken.fallon.nospam@nospam.gmail.com (Ken Fallon)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=30</link>
    <description><![CDATA[
In today's show Ken interviews Klaas-Jan Koopman about the Elfstedentocht a particularly Dutch phenomenon. He gives us some background to the tour and tells the story of his Father who has a permit to participate should it go ahead.




This interview was recorded yesterday and since then the organisation committee have said that the tour will not be going ahead this weekend as the ice is not thick enough. We can all wait and see together if it happens or not.
http://www.dutchnews.nl/news/archives/2012/02/poor_ice_growth_on_tuesday_nig.php


http://en.wikipedia.org/wiki/Elfstedentocht
Elfstedentocht

From Wikipedia, the free encyclopedia


The Elfstedentocht (or, in West Frisian, Alvestêdetocht, sometimes in English : Eleven Cities Tour), at almost 200 km, is the world's largest speed skating competition and leisure skating tour, and is held in the province of Friesland, Netherlands only when the ice along the entire course is 15 cm thick.


The tour, almost 200 km in length, is conducted on frozen canals, rivers and lakes between the eleven historic Frisian cities: Leeuwarden, Sneek, IJlst, Sloten, Stavoren, Hindeloopen, Workum, Bolsward, Harlingen, Franeker, Dokkum then returning to Leeuwarden. The tour is not held every year, mostly because not every Dutch winter permits skating on natural ice. The last editions were in 1985, 1986 and 1997. Adding to that, the tour currently features about 15,000 amateur skaters taking part, putting high requirements on the quality of the ice. There is a stated regulatory requirement for the race to take place that the ice must be (and remain at) a minimum thickness of 15 centimetres along the entirety of the course. All skaters must be a member of the Association of the Eleven Frisian Cities. A starting permit is required. Further more, in each city the skater must collect a stamp, as well as a stamp from the three secret check points. The skater must finish before midnight.
]]>
</description>
    <itunes:summary><![CDATA[
In today's show Ken interviews Klaas-Jan Koopman about the Elfstedentocht a particularly Dutch phenomenon. He gives us some background to the tour and tells the story of his Father who has a permit to participate should it go ahead.




This interview was recorded yesterday and since then the organisation committee have said that the tour will not be going ahead this weekend as the ice is not thick enough. We can all wait and see together if it happens or not.
http://www.dutchnews.nl/news/archives/2012/02/poor_ice_growth_on_tuesday_nig.php


http://en.wikipedia.org/wiki/Elfstedentocht
Elfstedentocht

From Wikipedia, the free encyclopedia


The Elfstedentocht (or, in West Frisian, Alvestêdetocht, sometimes in English : Eleven Cities Tour), at almost 200 km, is the world's largest speed skating competition and leisure skating tour, and is held in the province of Friesland, Netherlands only when the ice along the entire course is 15 cm thick.


The tour, almost 200 km in length, is conducted on frozen canals, rivers and lakes between the eleven historic Frisian cities: Leeuwarden, Sneek, IJlst, Sloten, Stavoren, Hindeloopen, Workum, Bolsward, Harlingen, Franeker, Dokkum then returning to Leeuwarden. The tour is not held every year, mostly because not every Dutch winter permits skating on natural ice. The last editions were in 1985, 1986 and 1997. Adding to that, the tour currently features about 15,000 amateur skaters taking part, putting high requirements on the quality of the ice. There is a stated regulatory requirement for the race to take place that the ice must be (and remain at) a minimum thickness of 15 centimetres along the entirety of the course. All skaters must be a member of the Association of the Eleven Frisian Cities. A starting permit is required. Further more, in each city the skater must collect a stamp, as well as a stamp from the three secret check points. The skater must finish before midnight.
]]>
</itunes:summary>
    <pubDate>Wed, 08 Feb 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0919.mp3" length="10821357" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0919.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>

  </channel>
</rss>

