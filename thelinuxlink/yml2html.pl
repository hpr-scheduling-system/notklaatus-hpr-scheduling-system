#!/usr/bin/perl
# ÇirçösÚáóíéőöÓÁśł
use strict;
use warnings;
use utf8;
use YAML::Syck;
use Data::Dumper::Simple;
binmode STDOUT, ":utf8";
 use Digest::MD5 qw(md5 md5_hex md5_base64);

my %podcasts;
my $ymlfile= $ARGV[0] || "podcast-master.yml";
my $path_to_qrcode = "http://kenfallon.com/files/qrcodes/";
# grep "^http" podcast-master.yml | awk -F ':' '{print $1":"$2}' | while read i;do qrencode "$i" -o $(echo $i | md5sum | cut -c -32).png;done
# echo $(echo http://hackerpublicradio.org | md5sum | cut -c -32).png

if ( -e "$ymlfile" ) 
{
    print STDERR "Loading processed podcast hash $ymlfile.\n";
    my $yaml = LoadFile($ymlfile);
    %podcasts = %$yaml;
}

#print Dumper (%podcasts);
my @lastrelease = ();
for my $show ( sort keys %podcasts ) 
{
  my $date = "1999-01-01T00:00:00";
  if ( exists $podcasts{"$show"}{"lastshow"} ) {
    $date = $podcasts{"$show"}{"lastshow"};
  }
  push ( @lastrelease, $date . "," . $show );
}

@lastrelease = reverse sort ( @lastrelease );

open my $FH, ">:utf8", "booklet.html";
print $FH '<html>
<style type="text/css">
p.sample {
         font-family: sans-serif;
         font-style: normal;
         font-variant: normal;
         font-weight: normal;
         font-size: small;
         line-height: 100%;
         word-spacing: normal;
         letter-spacing: normal;
         text-decoration: none;
         text-transform: none;
         text-align: left;
         text-indent: 0ex;
}
</style>
</head>
<body>';
print $FH '<p class="sample"><strong>The Linux Link</strong> (<em><a href="http://www.thelinuxlink.net/">http://www.thelinuxlink.net/</a></em>) The following list is based on information on the <strong>The Linux Link</strong> website, maintained by Dann Washko. Thanks to everyone who supplied updated information on the podcasts and a special thank you to Dave Morriss for helping to write the script that parsed the links. The shows are listed based on their last release date.</p><hr />' . "\n";
print $FH '<p class="sample">' . "\n";
my @other = "";
foreach my $show ( @lastrelease )
{
  my ( $date, $title, $desc, $link );
  ( $date, $show ) = split ( ",", $show );
  if (
    ( exists $podcasts{"$show"}{"link"} ) && 
    ( exists $podcasts{"$show"}{"title"} ) && 
    ( exists $podcasts{"$show"}{"description"} ) 
  ){
    print $FH '<strong>' . $podcasts{"$show"}{"title"} . '</strong> (<em><a href="' . $podcasts{"$show"}{"link"} . '">' . $podcasts{"$show"}{"link"} . '</a></em>) ' . $podcasts{"$show"}{"description"} . " <em>($date)</em><br /><br />\n";
  }
  else
  {
    push ( @other, $show );
  }
}
print $FH '</p>';
print $FH '</body>
</html>';
close $FH;


@lastrelease = ();
for my $show ( sort keys %podcasts ) 
{
  if ( 
  ( exists $podcasts{"$show"}{"link"} ) && 
  ( exists $podcasts{"$show"}{"title"} ) && 
  ( exists $podcasts{"$show"}{"description"} ) &&  
  ( exists $podcasts{"$show"}{"lastshow"} )
  )
  {
    #print $podcasts{"$show"}{"lastshow"} . "\t" . $podcasts{"$show"}{"link"}  . "\t" . $podcasts{"$show"}{"title"} . "\t" .$podcasts{"$show"}{"description"} . "\n"; 
    push ( @lastrelease, $podcasts{"$show"}{"lastshow"} . "," . $show );
  }
  if ( exists $podcasts{"$show"}{"description"} )
  {
    $podcasts{"$show"}{"description"} =~ s/\r\n/ /g;
    $podcasts{"$show"}{"description"} =~ s/\n/ /g;
    $podcasts{"$show"}{"description"} =~ s/  / /g;
  }
}


@lastrelease = reverse sort ( @lastrelease );

open my $BH, ">:utf8", "wordpress.html";
print $BH '<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta charset="UTF-8" />
<title>TheLinuxLink.net</title>
<link rel="stylesheet" type="text/css" media="all" href="http://kenfallon.com/wp-content/themes/twentyten/style.css" />
</html>
<body>';
print $BH '<h1>The Linux Link</h1>' . "\n";
print $BH '<p style="text-align: left;">' . "\n";
print $BH '<a href="http://www.thelinuxlink.net/"><img class="alignright" title="QRCode to The Linux Link: Portal To Linux Web Radio Shows" src="http://kenfallon.com/files/qrcodes/thelinuxlink.png" width="144" />' . "\n";
print $BH '<a href="http://www.thelinuxlink.net/"><img class="alignleft" title="The Linux Link Image" src="http://www.thelinuxlink.net/images/world_penguin_grad_lbolt.png" width="144" /></a>' . "\n";
print $BH 'The following list is based on information on the <a href="http://www.thelinuxlink.net/"><strong>The Linux Link</strong></a> website, maintained by Dann Washko. Thanks to everyone who supplied updated information on the podcasts and a special thank you to Dave Morriss for helping to write the script that parsed the links.  The shows are listed based on their last release date.
<!-- The application can be found here https://gitorious.org/hpr-scheduling-system/hpr-scheduling-system/trees/master/thelinuxlink -->
' . "\n";
print $BH '<em><a href="http://www.thelinuxlink.net/">http://www.thelinuxlink.net/</a></em></p>' . "\n";
print $BH '<hr />' . "\n";

foreach my $show ( @lastrelease )
{
  my ( $date, $left_image, $right_image, $show_image, $qr_image );
  ( $date, $show ) = split ( ",", $show );
  print $BH '<h2>' . $podcasts{"$show"}{"title"} . '</h2>' . "\n";
  print $BH '<p style="text-align: left;">' . "\n";
  $qr_image = md5_hex($show). '.png';
 `qrencode $show -o ./qrcodes/$qr_image`;
  print $BH '<a href="' . $podcasts{"$show"}{"link"} . '"><img class="alignright" title="qrcode to ' . $podcasts{"$show"}{"title"} . '" src="' . $path_to_qrcode . $qr_image . '" width="144" /></a>' . "\n";

  if ( exists $podcasts{"$show"}{"image"} )
  {
    print $BH '<a href="' . $podcasts{"$show"}{"link"} . '"><img class="alignleft" title="' . $podcasts{"$show"}{"title"} . ' Image" src="' . $podcasts{"$show"}{"image"} . '" width="144" /></a>' . "\n";
  }
  elsif ( exists $podcasts{"$show"}{"itunes:image"} )
  {
    print $BH '<a href="' . $podcasts{"$show"}{"link"} . '"><img class="alignleft" title="' . $podcasts{"$show"}{"title"} . ' Image" src="' . $podcasts{"$show"}{"itunes:image"} . '" width="144" /></a>' . "\n";
  }
  else
  {
    print $BH ' ' . "\n";
  }
  print $BH $podcasts{"$show"}{"description"} . " <em>($date)</em><br /><br />\n";
  print $BH '<em><a href="' . $podcasts{"$show"}{"link"} . '">' . $podcasts{"$show"}{"link"} . '</a></em></p>' . "\n";
  print $BH '<hr />' . "\n";
}
print $BH '<hr /> <h2>Archived Shows </h2><p class="sample">' . "\n";
foreach my $show ( @other )
{
  print $BH '<a href="' . $show . '">' . $show . "</a><br />\n";
}
print $BH '</p>';
print $BH '</body>
</html>';
close $BH;