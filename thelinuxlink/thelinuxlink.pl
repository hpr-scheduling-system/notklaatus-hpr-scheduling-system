#!/usr/bin/perl
#===============================================================================
# Copyright:
#  Ken Fallon <ken.fallon@gmail.com>
#  Dave Morriss <dave.morriss@gmail.com>
#
# Released under the terms of the GNU Affero General Public License (AGPLv3)
# http://www.gnu.org/licenses/agpl-3.0.html
#
# This project is hosted at
# https://gitorious.org/hpr-scheduling-system/
#
#
#===============================================================================
# UTF-8 check -> ÇirçösÚáóíéőöÓÁśł <-
# TODO atom support
use strict;
use warnings;
use utf8;

binmode STDOUT, ":utf8";

#===============================================================================
# :REMARK    :16/11/2012 22:48:54:djm:
# See below for why we're doing this.
#===============================================================================
our $debug;
our @urls;

BEGIN { $debug = 9 }

# A hash of the database listing all the information we gather and save
our %podcasts;

#===============================================================================
# :REMARK    :16/11/2012 22:49:34:djm:
# The 'use' stuff normally placed at the start of a script is actually run as
# if it's in a BEGIN{} block. This means it's actioned before the Perl parser
# even gets to the main code. Your module checking code has to also be in
# a BEGIN otherwise it's processed too late.
# Things are even more complicated because you use a global variable '$debug'.
# I made it an 'our' variable above, set it (using the package syntax that
# makes it clear it's in the 'main' package) then set it to the value 9. That
# way it's available with that value both in the BEGIN block and in the main
# code.
# Phew! Perl gets gnarly at times!
#===============================================================================
# Check for required modules
BEGIN {
    my @modules = qw(
        Data::Dumper::Simple
        YAML::Syck
        DateTime::Format::ISO8601
        IO::Socket
        URI
        Getopt::Long
        LWP::Simple
    );

    my @missing;
    for (@modules) {
        eval "use $_";
        if ($@) {
            push( @missing, $_ );
        }
        else {
            if ( $debug >= 9 ) {
                print STDERR "DEBUG: Found required module $_\n";
            }
        }
    }
    if (@missing) {
        print STDERR "The following modules are needed in order for",
            "$0 to run:\n\t";
        print STDERR join( "\n\t", @modules ), "\n\n";
        print STDERR "Please install missing modules: "
            . join( ", ", @missing ) . "\n";
        exit;
    }
}

#
# Script name
#
( my $PROG = $0 ) =~ s|.*/||mx;

#
# Options and arguments
#
my %options;
Options( \%options );

if ( $options{'help'} ) {
    help();
    exit;
}

# Check the database is the first argument and everything is OK.
my $database = shift;
unless ($database) {
    usage();
    exit;
}

if ( $database =~ m|http(s?)://|i ) {
    &usage;
    print STDERR
        "You need to supply a YAML file as the first argument and not the url \"${database}\"\n";
    exit;
}

if ( -e "${database}" ) {
    Debug( 9, "DEBUG: Found a file ${database}\n" );
    if ( -z ${database} ) {
        Debug(
            9, "DEBUG: Creating YAML database in existing empty file
            ${database}\n"
        );
    }
    else {
        Debug( 9, "DEBUG: Loading the file ${database}\n" );
        my $yaml = LoadFile( ${database} );
        eval { %podcasts = %$yaml; };
        die "The database file \"${database}\" is corrupt. Error" if $@;
        %podcasts = %$yaml;
    }
}
else {
    Debug( 9, "DEBUG: Creating new YAML database in ${database}\n" );
}

# Gather all the optional URL's from the command line and add them to the list
# taken from the database
foreach my $argument (@ARGV) {
    Debug( 9, "DEBUG: The url: ${argument} check will be added.\n" );
    if ( $argument !~ m|http(s?)://|i ) { $argument = "http://" . $argument }
    my $dt = DateTime->now( time_zone => 'UTC' );
    $podcasts{"$argument"}{"lastcheck"} = $dt->datetime() . "Z";
}

# Cycle through each of the entries and check each in turn
foreach my $this_podcast ( keys %podcasts ) {
    Debug( 9, "DEBUG: Processing $this_podcast.\n" );
    if ( &check_if_server_is_up("$this_podcast") == 0 ) {
        Debug( 5, "ERROR: $this_podcast is DOWN.\n" );
        next;
    }
    &check_content_type("$this_podcast");
}

#print Dumper( %podcasts );
exit;
#====================================================================
# Functions

sub check_content_type {
    my ($content_type, $doc_length, $mod_time, $expires, $server);

    if (($content_type, $doc_length, $mod_time, $expires, $server) =
        head("$_[0]")) {
        Debug( 9, "DEBUG: \"$_[0]\" is \"$content_type\".\n" );
        if ( $content_type =~ m|html|i ) { 
            return 1; 
        } 
        elsif ( $content_type =~ m|xml|i ) { 
            return 2; 
        }
        else {
            return 0;
        }
    } else {
        Debug( 9, "DEBUG: \"$_[0]\" is not available.\n" );
        return 404;
    }
} # check_content_type

sub check_if_server_is_up {
    my $url      = URI->new("$_[0]");
    my $hostname = $url->host();
    my $port     = $url->port();
    my $socket   = eval {
        return IO::Socket::INET->new(
            Proto    => "tcp",
            PeerAddr => $hostname,
            PeerPort => $port,
            Reuse    => 1,
            Timeout  => 10
        ) or return 0;
    };
    if ($socket) {
        Debug( 9, "DEBUG: \"$hostname\" on port \"$port\" is UP.\n" );
        eval { return $socket->close; };
        return 1;
    }
    else {
        Debug( 9, "DEBUG: \"$hostname\" on port \"$port\" is DOWN.\n" );
        return 0;
    }
}    # check_if_server_is_up

sub usage {
    Debug( 9, "DEBUG: Stepped into usage\n" );
    print "Usage: $PROG YAMLFILE [--help] [URL]... \n";
}    # usage

sub help {
    Debug( 9, "DEBUG: Stepped into help\n" );
    &usage;
    print <<ENDHELP;

eg: $PROG podcasts.yaml http://hackerpublicradio.org http://example.com/feed.xml

Checks the status of a list of podcasts, saving the results in a YAML file.
The application is intended to be run frequently so that the YAML file is up to date.
The YAML file can then be parsed to different output formats.

The first argument must be the YAML file name.
It will be created if it doesn't exist.

The optional list of URL's will be added to the YAML file.
If the URL point to a RSS or Atom feed then only those will be checked.
If the URL point to a non xml site it will be examined for links to feeds.
These are identified using the "link" element in the HTML head section.
  eg: <link rel="alternate" type="application/rss+xml" href="... />

All feeds that are found will be checked.
Each time the application is run the latest state is saved.
If a site that has been checked goes down, the existing information will be kept.

ENDHELP

}    # usage

#===  FUNCTION  ================================================================
#         NAME: Options
#      PURPOSE: Processes command-line options
#   PARAMETERS: $optref     Hash reference to hold the options
#      RETURNS: Undef
#  DESCRIPTION:
#       THROWS: no exceptions
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub Options {
    my ($optref) = @_;

    my @options = ( "help", "debug", );

    if ( !GetOptions( $optref, @options ) ) {
        usage;
        exit;
    }

    return;
}

sub Debug {
    my ( $level, $message ) = @_;

    if ( $debug >= $level ) {
        print STDERR $message;
    }
}

# use Term::ANSIColor;
# use XML::XPath;
# use LWP::Simple;
# use HTML::TreeBuilder;
# #use Date::Format;
# use DateTime::Format::RSS;
# require LWP::UserAgent;
# use XML::LibXML;
# our @urls;
# my ( @feeds, @thesefeeds );

# my $content;
# my $schema_file = "rss-2_0.xsd";

# # # foreach my $url (@urls)
# # #
# # #   my $ua = LWP::UserAgent->new;
# # #   $ua->timeout(10);
# # #
# # #   my $response = $ua->get( "$url" );
# # #   if ($response->is_success) {
# # #       $content = get $url;
# # #   }
# # #   else {
# # #       print $response->status_line . "\n";
# # #       $podcasts{"$url"}{"error"} = $response->status_line;
# # #       next;
# # #   }
# # #   if ( !defined $content )
# # #   {
# # #       print STDERR "Can't retrieve the url: $url\n";
# # #       next;
# # #   }
# # #   if ( ( $content =~ "<rss" ) && ( $content =~ "<enclosure" ) )
# # #   {
# # #     push( @ { $podcasts{ "$url" }{ "feeds" } }, $url );
# # #   }
# # #   else
# # #   {
# # #     my $h   = HTML::TreeBuilder->new_from_content($content);
# # #     my @rss = $h->look_down(
# # #         _tag => 'link',
# # #         rel  => 'alternate',
# # #         type => 'application/rss+xml'
# # #     );
# # #     foreach my $rss (@rss)
# # #     {
# # #         { push( @ { $podcasts{ "$url" }{ "feeds" } }, $rss->attr('href') ); }
# # #     }
# # #   }
# # # }
# # #
# # # for my $show ( sort keys %podcasts ) {
# # #   foreach my $thisfeed ( @{ $podcasts{ "$show" }{ "feeds" } })
# # #   {
# # #     if (
# # #       ( exists $podcasts{"$show"}{"link"} ) &&
# # #       ( exists $podcasts{"$show"}{"title"} ) &&
# # #       ( exists $podcasts{"$show"}{"description"} ) &&
# # #       ( ( exists $podcasts{"$show"}{"image"} ) || ( exists $podcasts{"$show"}{"itunes:image"} ) )
# # #       )
# # #     {
# # #       print "The record for $show is already complete\n";
# # #       if ( $debug == 0 ){
# # #         $podcasts{"$show"}{"feeds"} = ();
# # #       }
# # #       next;
# # #     };
# # #     if ( $thisfeed !~ "^http" ){
# # #       $thisfeed = $show . "/" . $thisfeed;
# # #     }
# # #     print "Processing the feed $thisfeed from $show\n";
# # #     my $content = get $thisfeed;
# # #     if ( !defined $content ) {
# # #         print "Can't retrieve the feed $thisfeed from $show\n";
# # #         next;
# # #     }
# # #     my $schema = XML::LibXML::Schema->new(location => $schema_file);
# # #     my $parser = XML::LibXML->new;
# # #     if( ! eval { $parser->parse_string($content) } ) {
# # #       print "RSS validation of $show failed\n";
# # #       $podcasts{"$show"}{"error"} = "RSS validation of $show failed " . $@;
# # #       next;
# # #     }
# # #     my $doc    = $parser->parse_string($content);
# # #     eval { $schema->validate($doc) };
# # #     if ( ( $content =~ "<rss" ) && ( $content =~ "<enclosure" ) )
# # #     {
# # #       my $xp;
# # #       eval {
# # #           local $SIG{'__DIE__'};
# # #           $xp = XML::XPath->new( xml => $content );
# # #       };
# # #       if ($@) {
# # #           warn "Caught error: $@";
# # #           next;
# # #       }
# # #       if ( $xp->exists('/rss/channel/image/url') ) {
# # #         $podcasts{"$show"}{"image"} = $xp->findvalue('/rss/channel/image/url')->string_value;
# # #         if ( $podcasts{"$show"}{"image"} !~ "^http" ){
# # #           $podcasts{"$show"}{"image"} = $show. "/" . $podcasts{"$show"}{"image"};
# # #         }
# # #       }
# # #       if ( $xp->exists('/rss/channel/link') ) {
# # #         $podcasts{"$show"}{"link"} = $xp->findvalue('/rss/channel/link')->string_value;
# # #       }
# # #       if ( $xp->exists('/rss/channel/title') ) {
# # #         $podcasts{"$show"}{"title"} = $xp->findvalue('/rss/channel/title')->string_value;
# # #       }
# # #       if ( $xp->exists('/rss/channel/description') ) {
# # #         $podcasts{"$show"}{"description"} = $xp->findvalue('/rss/channel/description')->string_value;
# # #       }
# # #       if ( $xp->exists('/rss/channel/itunes:image/@href') ) {
# # #         $podcasts{"$show"}{"itunes:image"} = $xp->findvalue('/rss/channel/itunes:image/@href')->string_value;
# # #       }
# # #       my $nodeset = $xp->find('/rss/channel/item/enclosure/@type');
# # #       if ( my @nodelist = $nodeset->get_nodelist )
# # #       {
# # #         @thesefeeds = map( $_->string_value, @nodelist );
# # #         foreach my $feedtype (@thesefeeds)
# # #         {
# # #             $podcasts{"$show"}{"$feedtype"} = $thisfeed;
# # #         }
# # #       }
# # #       $nodeset = $xp->find('/rss/channel/item/pubDate');
# # #       my @pubdates = ();
# # #       if ( my @nodelist = $nodeset->get_nodelist )
# # #       {
# # #         my @thesepubdates = map( $_->string_value, @nodelist );
# # #         foreach my $thispubdate (@thesepubdates)
# # #         {
# # #           my $fmt = DateTime::Format::RSS->new;
# # #
# # #           if( ! eval { $fmt->parse_datetime($thispubdate) } ) {
# # #             print "Date processing of $show failed\n";
# # #             $podcasts{"$show"}{"error"} = "Date processing of $show failed " . $@;
# # #             next;
# # #           }
# # #           my $dt  = $fmt->parse_datetime($thispubdate);
# # #           my $str = $fmt->format_datetime($dt);
# # #           push ( @pubdates, $str);
# # #         }
# # #       }
# # #       @pubdates = sort(@pubdates);
# # #       $podcasts{"$show"}{"lastshow"} = $pubdates[$#pubdates];
# # #       if ( $debug == 0 ){
# # #         $podcasts{"$show"}{"feeds"} = ();
# # #       }
# # #     }
# # #   }
# # # }
# # # print Dumper (%podcasts);
# # # open my $FH, ">:utf8", "podcast-hash.yml";
# # # DumpFile( $FH, \%podcasts );
# # # close($FH);

# Functions below here

# vim: syntax=perl:ts=8:sw=4:et:ai:tw=78:fo=tcrqn21:fdm=marker
