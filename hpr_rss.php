<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" >
<channel>
  <title>Hacker Public Radio</title>
  <link>http://hackerpublicradio.org/about.php</link>
  <itunes:subtitle>A daily show hosted the community on topics that are of interest to hackers and hobbyists.</itunes:subtitle>
  <description>Hacker Public Radio is an podcast that releases shows every weekday Monday through Friday. Our shows are produced by the community (you) and can be on any topic that is "are of interest to hackers and hobbyists".</description>
  <language>en-us</language>
  <itunes:category text="Technology">
    <itunes:category text="Tech News"/>
  </itunes:category>
  <itunes:category text="Education">
    <itunes:category text="Training"/>
  </itunes:category>
  <itunes:image href="http://hackerpublicradio.org/images/hpr_feed.png"/>
  <itunes:explicit>yes</itunes:explicit>
  <itunes:keywords>Community Radio, Tech Interviews, Linux, Open, Hobby, Software Freedom</itunes:keywords>
  <copyright>Creative Commons Attribution-ShareAlike 3.0 License</copyright>
  <managingEditor>feedback@NOSPAM-hackerpublicradio.org (HPR Feedback)</managingEditor> 
  <itunes:owner>    
    <itunes:name>HPR Webmaster</itunes:name>
    <itunes:email>admin@hackerpublicradio.org</itunes:email>
  </itunes:owner>
  <webMaster>admin@NOSPAM-hackerpublicradio.org (HPR Webmaster)</webMaster> 
  <generator>kate</generator> 
  <docs>http://www.rssboard.org/rss-specification</docs>
  <ttl>3200</ttl>
  <skipDays>
    <day>Saturday</day>
    <day>Sunday</day>
  </skipDays>
  <image>
    <url>http://hackerpublicradio.org/images/hpr_feed_small.png</url>
    <title>Hacker Public Radio</title>
    <link>http://hackerpublicradio.org/about.php</link>
    <description>The Hacker Public Radio Old Microphone Logo</description>
    <height>96</height>
    <width>120</width>
  </image>
  <atom:link href="http://hackerpublicradio.org/hpr_mp3_rss.php" rel="self" type="application/rss+xml" />
  <pubDate>Sat, 07 Apr 2012 00:00:00 +0000</pubDate>
  <item>
    <title>HPR0960: TGTM Newscast for 2012/04/04 </title>
    <author>hpr.nospam@nospam.deepgeek.us  (deepgeek)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=73</link>
    <description><![CDATA[TGTM Newscast for 2012/4/4 
Here is a news review:

  EFF Again Reminds Court Forced Warrantless DNA Collection Violates Fourth Amendment
  U.S. Escalates Drone War in Yemen
  Amnesty Accuses Cuban Government of Harassing 
  Colia Clark Wins NY Green Party Nomination for US Senate
  On financial blockades as a political tool for censorship 
  French Constitutional Court Bans Law Enforcement Use of National Biometric ID Database
  NSA Chief Appears to Deny Ability to Warrantlessly Wiretap Despite Evidence 
  Trademark Lawyers Push For Crazy New Domain Rules Making It Easy For Them To Take Away Others' Domains
  AMD Launches New Platform for Dedicated Web Hosting Providers 
  RapidShare Declared Legal In Court, With a Twist

Other Headlines:

  The Root of All Evil? The Dollar, the BRICS and South Africa
  Hanford contractors admit big safety problems remain
  The Polite Conference Rooms Where Liberties Are Saved and Lost
  Courthouse News Service on Hedge's NDAA suit against Obama
   Power Management of Online Data-Intensive Services

News from &quot;techdirt.com,&quot; Audio of &quot;Moment of Clarity #126,&quot; &quot;havanatimes.org,&quot;&amp;nbsp; and &quot;allgov.com&quot; used under&amp;nbsp;arranged permission. News from&amp;nbsp;&quot;eff.org&quot;&amp;nbsp; and &quot;torrentfreak.com&quot; used under permission of the Creative Commons by-attribution&amp;nbsp;license. News from &quot;gpnys.com,&quot; and &quot;amd.com&quot; are press releases. News from &quot;wlcentral.org&quot; used under permission of the Creative Commons by-attribution non-commercial no-derivatives license. News Sources retain their respective copyrights.

Links

https://www.eff.org/deeplinks/2012/03/eff-again-reminds-court-forced-warrantless-dna-collection-violates-fourth
http://www.allgov.com//ViewNews/US_Escalates_Drone_War_in_Yemen_120331
http://www.havanatimes.org/?p=65391
http://www.web.gpnys.com/?p=11728
http://wlcentral.org/node/2518
https://www.eff.org/deeplinks/2012/03/french-constitutional-court-bans-law-enforcement-use-biometric-data
https://www.eff.org/deeplinks/2012/03/nsa-chief-denies-ability-warrantlessly-wiretap-despite-evidence
http://www.techdirt.com/articles/20120324/01292018233/trademark-lawyers-push-crazy-new-domain-rules-making-it-easy-them-to-take-away-others-domains.shtml
http://www.amd.com/us/press-releases/Pages/amd-launches-new-platform-2012mar20.aspx
http://torrentfreak.com/rapidshare-declared-legal-in-court-with-a-twist-120327/                                                                                                                                
http://sacsis.org.za/site/article/1253                                                       
http://seattletimes.nwsource.com/html/localnews/2017819435_hanford23m.html                                                                                                                                          
http://www.truthdig.com/report/item/the_polite_conference_rooms_where_liberties_are_saved_and_lost_20120326/                                                                      
http://www.courthousenews.com/2012/03/28/45128.htm                               
http://perspectives.mvdirona.com/2012/03/29/PowerManagementOfOnlineDataIntensiveServices.aspx
]]>
</description>
    <itunes:summary><![CDATA[TGTM Newscast for 2012/4/4 
Here is a news review:

  EFF Again Reminds Court Forced Warrantless DNA Collection Violates Fourth Amendment
  U.S. Escalates Drone War in Yemen
  Amnesty Accuses Cuban Government of Harassing 
  Colia Clark Wins NY Green Party Nomination for US Senate
  On financial blockades as a political tool for censorship 
  French Constitutional Court Bans Law Enforcement Use of National Biometric ID Database
  NSA Chief Appears to Deny Ability to Warrantlessly Wiretap Despite Evidence 
  Trademark Lawyers Push For Crazy New Domain Rules Making It Easy For Them To Take Away Others' Domains
  AMD Launches New Platform for Dedicated Web Hosting Providers 
  RapidShare Declared Legal In Court, With a Twist

Other Headlines:

  The Root of All Evil? The Dollar, the BRICS and South Africa
  Hanford contractors admit big safety problems remain
  The Polite Conference Rooms Where Liberties Are Saved and Lost
  Courthouse News Service on Hedge's NDAA suit against Obama
   Power Management of Online Data-Intensive Services

News from &quot;techdirt.com,&quot; Audio of &quot;Moment of Clarity #126,&quot; &quot;havanatimes.org,&quot;&amp;nbsp; and &quot;allgov.com&quot; used under&amp;nbsp;arranged permission. News from&amp;nbsp;&quot;eff.org&quot;&amp;nbsp; and &quot;torrentfreak.com&quot; used under permission of the Creative Commons by-attribution&amp;nbsp;license. News from &quot;gpnys.com,&quot; and &quot;amd.com&quot; are press releases. News from &quot;wlcentral.org&quot; used under permission of the Creative Commons by-attribution non-commercial no-derivatives license. News Sources retain their respective copyrights.

Links

https://www.eff.org/deeplinks/2012/03/eff-again-reminds-court-forced-warrantless-dna-collection-violates-fourth
http://www.allgov.com//ViewNews/US_Escalates_Drone_War_in_Yemen_120331
http://www.havanatimes.org/?p=65391
http://www.web.gpnys.com/?p=11728
http://wlcentral.org/node/2518
https://www.eff.org/de]]>
</itunes:summary>
    <pubDate>Fri, 06 Apr 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0960.mp3" length="11503830" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0960.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0959: The Orca Screen Reader</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (Various Hosts)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=109</link>
    <description><![CDATA[Joanmarie Diggs talk entitled &quot;The Orca Screen Reader, how it does what it does and how you can help&quot;
Joanmarie Diggs is the Lead Developer for Orca and this talk was recorded at the Northeast GNU/Linux Fest 2012-03-17]]>
</description>
    <itunes:summary><![CDATA[Joanmarie Diggs talk entitled &quot;The Orca Screen Reader, how it does what it does and how you can help&quot;
Joanmarie Diggs is the Lead Developer for Orca and this talk was recorded at the Northeast GNU/Linux Fest 2012-03-17]]>
</itunes:summary>
    <pubDate>Thu, 05 Apr 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0959.mp3" length="17794028" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0959.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0958: KDE Gathering-Plasma Active-THE Tablet</title>
    <author>davidglennwhitman.nospam@nospam.gmail.com (David Whitman)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=209</link>
    <description><![CDATA[
KDE will get hosting a regional meeting of KDE for the Northwestern United States April 28 and 29, 2012 at LinuxFest Northwest 
http://linuxfestnorthwest.org


Akademy 2012
30th June - 6th July 2012, Tallinn, Estonia
http://akademy.kde.org/


KDE is 15 years old
Kool Desktop Environment


KDE desktop is called the Plasma Workspace
Plasma Workspaces is the umbrella term for all graphical environments provided by KDE. (from Wikipedia)


Owncloud 
http://owncloud.org/


Krita - Painting and Image Editing
http://www.kde.org/applications/graphics/krita/


This OS is open unlike other tablet operating systems.
plasma-active.org


http://makeplaylive.com/
Vivaldi Tablet


Anyone can attend this KDE gathering which is co-located with LinuxFest Northwest
Plasma Active is not locked down and has office applications


Calligra 
http://www.calligra-suite.org/
Word Processor,spreadsheet presentation software, drawing optimized for touch


Calendaring, PIM aspect to KDE  has been refocused to touch and is avaiable right now


Some KDE programs are still being optimized for the touch environment


Qt-questions about it's openness has been resolved


Might be some Raspberry PI's at the gathering and they will be raffled after the KDE coders get done with them at the LinuxFest Northwest world famous raffle.


You can make your own tablet and use the OS for your project.


OS uses Qt and C++


QT Quick
http://qt.nokia.com/qtquick/ 


A continuation of Megoo - Mer
http://merproject.org/


Can be used on some smart phones


basyskom.com


Check out KDE and Plasma Active


These notes based on the interview by David Whitman with Carl Symons and John Blanford for Hacker Public Radio. 

]]>
</description>
    <itunes:summary><![CDATA[
KDE will get hosting a regional meeting of KDE for the Northwestern United States April 28 and 29, 2012 at LinuxFest Northwest 
http://linuxfestnorthwest.org


Akademy 2012
30th June - 6th July 2012, Tallinn, Estonia
http://akademy.kde.org/


KDE is 15 years old
Kool Desktop Environment


KDE desktop is called the Plasma Workspace
Plasma Workspaces is the umbrella term for all graphical environments provided by KDE. (from Wikipedia)


Owncloud 
http://owncloud.org/


Krita - Painting and Image Editing
http://www.kde.org/applications/graphics/krita/


This OS is open unlike other tablet operating systems.
plasma-active.org


http://makeplaylive.com/
Vivaldi Tablet


Anyone can attend this KDE gathering which is co-located with LinuxFest Northwest
Plasma Active is not locked down and has office applications


Calligra 
http://www.calligra-suite.org/
Word Processor,spreadsheet presentation software, drawing optimized for touch


Calendaring, PIM aspect to KDE  has been refocused to touch and is avaiable right now


Some KDE programs are still being optimized for the touch environment


Qt-questions about it's openness has been resolved


Might be some Raspberry PI's at the gathering and they will be raffled after the KDE coders get done with them at the LinuxFest Northwest world famous raffle.


You can make your own tablet and use the OS for your project.


OS uses Qt and C++


QT Quick
http://qt.nokia.com/qtquick/ 


A continuation of Megoo - Mer
http://merproject.org/


Can be used on some smart phones


basyskom.com


Check out KDE and Plasma Active


These notes based on the interview by David Whitman with Carl Symons and John Blanford for Hacker Public Radio. 

]]>
</itunes:summary>
    <pubDate>Tue, 03 Apr 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0958.mp3" length="11172155" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0958.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0957: Freedom is not Free 3 - Documentation</title>
    <author>zwilnik.nospam@nospam.zwilnik.com (Ahuka)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=198</link>
    <description><![CDATA[https://ohiolinux.org/node/186
http://www.zwilnik.com]]>
</description>
    <itunes:summary><![CDATA[https://ohiolinux.org/node/186
http://www.zwilnik.com]]>
</itunes:summary>
    <pubDate>Mon, 02 Apr 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0957.mp3" length="21946978" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0957.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0956: HPR Community News for Feb 2012</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (HPR Admins)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=159</link>
    <description><![CDATA[HPR Community News

A monthly look at what has been going on in the HPR community. This is a regular show scheduled for the first Monday of the month.
New hosts

Welcome to our new host: 
Digital Maniac, 
David Whitman, 
Neodragon, and all the 
ZombieMasters.

If you would like to become a HPR host then please head over to http://hackerpublicradio.org/contribute.php

Show Review


id
title
host


        
936
Monthly Review show 2012 Feb
HPR Admins

            
937
How I started with linux
riddlebox

            
938
Cloning Windows WiFi Profiles and Installing Skype...
FiftyOneFifty

            
939
Sunday Morning Linux Review: Episode 021
HPR Admins

            
940
TGTM Tech News for 2012-03-07
deepgeek

            
941
Whats in my bag / Portable Apps
Digital Maniac

            
942
Zentyal Linux Small Business Server
riddlebox

            
943
Freedom is not Free 2 - Bugs
Ahuka

            
944
LITS: Episode 002 - tr
Dann

            
945
TGTM Tech News for 2012-03-14
deepgeek

            
946
HPR Interview David Whitman with Carl Symons and J...
David Whitman

            
947
Presentation by Jared Smith at the Columbia Area L...
Neodragon

            
948
Exchanging Data Podcast 2
dmfrey

            
949
The cchits 2011 overview
HPR Admins

            
950
TGTM Newscast for 2012/03/21 
deepgeek

            
951
Roku XD box
riddlebox

            
952
How I cut The Cable Cord Part 2
BrocktonBob

            
953
LITS: Episode 003 - cut
Dann

            
954
All Things Chrome
Robin Catling

            
955
Zombie Circus 00 - Pilot
ZombieMaster

            



Other items
HPR site was down for a few hours on 2/March but Josh had it back in a few hours

David Whitman writes to say that he will be having a table at http://linuxfestnorthwest.org/sponsors and he is still looking for volunteers to help out or even be the 'Big Cheese'.


Some bad news from the HeliOS project, http://www.fixedbylinux.com/about



HPR Images, can you send your feedback to the list


Haxradio.com is airing HPR episodes regularly


Were we having FTP login Issues ?


NELF Talk


David Whitman made us buttons


HPR vetting policy relating to adult, political, etc....
We don't have one


Episode 1000 and 1024

We should come up with an idea to celebrate Ep1000 ?
Answer = YES


For episode 1000 we will be gathering a sample of community members emailing their congratulations but for episode 1024 :) 


FiftyOneFifty will be coordinating a EPIC &quot;live&quot; show so please email your contributions to ep1k@hackerpublicradio.org

]]>
</description>
    <itunes:summary><![CDATA[HPR Community News

A monthly look at what has been going on in the HPR community. This is a regular show scheduled for the first Monday of the month.
New hosts

Welcome to our new host: 
Digital Maniac, 
David Whitman, 
Neodragon, and all the 
ZombieMasters.

If you would like to become a HPR host then please head over to http://hackerpublicradio.org/contribute.php

Show Review


id
title
host


        
936
Monthly Review show 2012 Feb
HPR Admins

            
937
How I started with linux
riddlebox

            
938
Cloning Windows WiFi Profiles and Installing Skype...
FiftyOneFifty

            
939
Sunday Morning Linux Review: Episode 021
HPR Admins

            
940
TGTM Tech News for 2012-03-07
deepgeek

            
941
Whats in my bag / Portable Apps
Digital Maniac

            
942
Zentyal Linux Small Business Server
riddlebox

            
943
Freedom is not Free 2 - Bugs
Ahuka

            
944
LITS: Episode 002 - tr
Dann

            
945
TGTM Tech News for 2012-03-14
deepgeek

            
946
HPR Interview David Whitman with Carl Symons and J...
David Whitman

        ]]>
</itunes:summary>
    <pubDate>Sun, 01 Apr 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0956.mp3" length="36440010" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0956.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0955: Zombie Circus 00 - Pilot</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (ZombieMaster)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=211</link>
    <description><![CDATA[Zombie Circus 00 - Pilot
          Recorded: 20120204
          Cast: Azimuth, monsterb, pegwole, Peter64, Sndchaser, Threethirty
          Music: Beware The Dangers Of A Ghost Scorpion - Zombie Dance Party
          
          Links:
          Desura
          FUDCon: Fedora Users and Developers Conference
          Homeland Security deports tourists for Twitter jokes.
          Galaxy Zoo
          Moonbase Alpha
          NSA releases ultra-secure open source Android derivative.
          Planet Hunters
          Previous owners data found on 100 Motorola Xoom tablets sold online.
          Raspberry Pi
          Rebecca Black Linux
          Steam
          US bars friends over Twitter joke.
          WineHQ
          
          More info can be found at Zombie Circus
Links


http://www.musicalley.com/music/producers/producerLibrary/artistdetails.php?BandHash=772ecf06f0581a80b23f1d73a702afdf
http://www.desura.com/
http://fedoraproject.org/wiki/FUDCon
http://www.itworld.com/cloud-computing/245639/homeland-security-deports-tourists-twitter-jokes
http://www.galaxyzoo.org/
http://www.nasa.gov/offices/education/programs/national/ltp/games/moonbasealpha/index.html
http://androidcommunity.com/nsa-releases-ultra-secure-open-source-android-derivative-20120117/
http://www.planethunters.org/
http://tech2.in.com/news/tablets/previous-owners-data-found-on-100-motorola-xoom-tablets-sold-online/278922
http://www.raspberrypi.org/
http://sourceforge.net/projects/rebeccablackos/
http://store.steampowered.com/
http://www.thesun.co.uk/sol/homepage/news/4095372/Twitter-news-US-bars-friends-over-Twitter-joke.html
http://www.winehq.org/
http://oggcastplanet.org/zombiecircus/

]]>
</description>
    <itunes:summary><![CDATA[Zombie Circus 00 - Pilot
          Recorded: 20120204
          Cast: Azimuth, monsterb, pegwole, Peter64, Sndchaser, Threethirty
          Music: Beware The Dangers Of A Ghost Scorpion - Zombie Dance Party
          
          Links:
          Desura
          FUDCon: Fedora Users and Developers Conference
          Homeland Security deports tourists for Twitter jokes.
          Galaxy Zoo
          Moonbase Alpha
          NSA releases ultra-secure open source Android derivative.
          Planet Hunters
          Previous owners data found on 100 Motorola Xoom tablets sold online.
          Raspberry Pi
          Rebecca Black Linux
          Steam
          US bars friends over Twitter joke.
          WineHQ
          
          More info can be found at Zombie Circus
Links


http://www.musicalley.com/music/producers/producerLibrary/artistdetails.php?BandHash=772ecf06f0581a80b23f1d73a702afdf
http://www.desura.com/
http://fedoraproject.org/wiki/FUDCon
http://www.itworld.com/cloud-computing/245639/homeland-security-deports-tourists-twitter-jokes
http://www.galaxyzoo.org/
http://www.nasa.gov/offices/education/programs/national/ltp/games/moonbasealpha/index.html
http://androidcommunity.com/nsa-releases-ultra-secure-open-source-android-derivative-20120117/
http://www.planethunters.org/
http://tech2.in.com/news/tablets/previous-owners-data-found-on-100-motorola-xoom-tablets-sold-online/278922
http://www.raspberrypi.org/
http://sourceforge.net/projects/rebeccablackos/
http://store.steampowered.com/
http://www.thesun.co.uk/sol/homepage/news/4095372/Twit]]>
</itunes:summary>
    <pubDate>Thu, 29 Mar 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0955.mp3" length="66456389" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0955.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0954: All Things Chrome</title>
    <author>fullcirclepodcast.nospam@nospam.googlemail.com (Robin Catling)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=160</link>
    <description><![CDATA[Cast your minds back to Summer 2011, when Google Plus still looked like a good idea, before the HP Touchpad came and went in a fire sale and before the Euro debt crisis turned into a Keystone Cops movie.

A presenter formerly of this parish, one Ed Hewitt, went out and bought himself a new toy; a Samsung Chromebook. ChromeOS marches on, but for how long? I stand back and referee as Ed and Dave Wilkins, fight it out.

The Full Circle Podcast is the companion to Full Circle Magazine, the Independent Magazine for the Ubuntu Community
Find us at www.fullcirclemagazine.org/podcast.
Feedback; you can post comments and feedback on the podcast page at fullcirclemagazine.org/podcast, send us a comment to podcast (at) fullcirclemagazine.org

Your Hosts:


Robin Catling (blog at http://catlingmindswipe.blogspot.com/, @robincatling on Twitter)
Ed Hewitt (blog at http://www.edhewitt.co.uk/, @edhewitt on Twitter)
Dave Wilkins (…had to go cook the dinner. @davidawilkins on Twitter)


Additional audio by Victoria Pritchard

Runtime: 24mins 0seconds
]]>
</description>
    <itunes:summary><![CDATA[Cast your minds back to Summer 2011, when Google Plus still looked like a good idea, before the HP Touchpad came and went in a fire sale and before the Euro debt crisis turned into a Keystone Cops movie.

A presenter formerly of this parish, one Ed Hewitt, went out and bought himself a new toy; a Samsung Chromebook. ChromeOS marches on, but for how long? I stand back and referee as Ed and Dave Wilkins, fight it out.

The Full Circle Podcast is the companion to Full Circle Magazine, the Independent Magazine for the Ubuntu Community
Find us at www.fullcirclemagazine.org/podcast.
Feedback; you can post comments and feedback on the podcast page at fullcirclemagazine.org/podcast, send us a comment to podcast (at) fullcirclemagazine.org

Your Hosts:


Robin Catling (blog at http://catlingmindswipe.blogspot.com/, @robincatling on Twitter)
Ed Hewitt (blog at http://www.edhewitt.co.uk/, @edhewitt on Twitter)
Dave Wilkins (…had to go cook the dinner. @davidawilkins on Twitter)


Additional audio by Victoria Pritchard

Runtime: 24mins 0seconds
]]>
</itunes:summary>
    <pubDate>Wed, 28 Mar 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0954.mp3" length="17377408" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0954.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0953: LITS: Episode 003 - cut</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (Dann)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=7</link>
    <description><![CDATA[
In the third in his series Dann, shows us the benifits of the cut command:



The cut command, as the man page states, &quot;removes sections from each line of a file.&quot; The cut command can also be used on a stream and it can do more than just remove section.  If a file is not specified or &quot;-&quot; is used, the cut command takes input from standard in. The cut command can be used to extract sections from a file or stream based upon a specific criteria.  An example of this would be cutting specific fields from a csv (comma separated values) file.  For instance, cut can be used to extract the name and email address from a csv file with the following content:



http://www.linuxintheshell.org/2012/03/28/episode-003-cut/ for the complete shownotes, including video.
]]>
</description>
    <itunes:summary><![CDATA[
In the third in his series Dann, shows us the benifits of the cut command:



The cut command, as the man page states, &quot;removes sections from each line of a file.&quot; The cut command can also be used on a stream and it can do more than just remove section.  If a file is not specified or &quot;-&quot; is used, the cut command takes input from standard in. The cut command can be used to extract sections from a file or stream based upon a specific criteria.  An example of this would be cutting specific fields from a csv (comma separated values) file.  For instance, cut can be used to extract the name and email address from a csv file with the following content:



http://www.linuxintheshell.org/2012/03/28/episode-003-cut/ for the complete shownotes, including video.
]]>
</itunes:summary>
    <pubDate>Wed, 28 Mar 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0953.mp3" length="11071626" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0953.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0952: How I cut The Cable Cord Part 2</title>
    <author>bhpcrepair.nospam@nospam.gmail.com (BrocktonBob)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=202</link>
    <description><![CDATA[
Hello HPR,
Just BrocktonBob here again with part 2 of How I Cut The Cable Cord.
In this episode I talk about adding a second settop box,and getting the Playon Server software on a computer so you can get alot more content.I also talk to you about adding an external harddrive.And how I made my own HD tv antenna.
]]>
</description>
    <itunes:summary><![CDATA[
Hello HPR,
Just BrocktonBob here again with part 2 of How I Cut The Cable Cord.
In this episode I talk about adding a second settop box,and getting the Playon Server software on a computer so you can get alot more content.I also talk to you about adding an external harddrive.And how I made my own HD tv antenna.
]]>
</itunes:summary>
    <pubDate>Tue, 27 Mar 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0952.mp3" length="5897288" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0952.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>
  <item>
    <title>HPR0951: Roku XD box</title>
    <author>admin.nospam@nospam.hackerpublicradio.org (riddlebox)</author>
    <link>http://hackerpublicradio.org/correspondents.php?hostid=94</link>
    <description><![CDATA[
I recently bought a roku XD box and I want to do a little review for you guys. First I would like to say that we have basic cable, and werent really looking to become &quot;cord cutters&quot;. You can check out the roku site at www.roku.com.




First I bought it from Best buy for 79.99 I think it was 84 dollars with tax. First I cant believe how tiny this thing is! When you open the box, you have the device, a remote and some RCA AV Cables. The XD also only works with wireless internet. The one I bought does have a HDMI port on it. It works great and does Netflix and other services like Amazon and Hulu Plus. 


You sign up for a roku account on the website and associate the roku box to that account. Which is close to what you do with most media type boxes now a days like blueray players and stuff. The only difference is those devices only give you content from a couple places. The roku actually has channels, that you can add to your roku box. I have added many channels on it and I am watching lots of content from the web. With services like Popcorn flix, which shows you movies and during those movies there are some commercials. They arent really that bad its like 1 commercial when they show it. It is always the same commercial though. Which kinda gets annoying. 


I use Netflix, and Crackle on it was well. There are lots of news channels like NBC also I know that MLB and I think NHL have channels too. I was amazed at the selection of channels on the device. It would be nice if the roku site had a listing of all private channels. As it is hard to search the internet to find a list, then add the code on the roku site only to find out that the channel code doesnt work anymore. There really are so many channels that you can add it is hard to explain them all. 


My wife and I both have android phones and we installed the roku remote control app which works great! As long as you are on the same wifi network as the roku you can control it. My wife is using the roku box for netflix and other services more than our blueray players because she likes the interface to netflix better. She says that just getting around the netflix channel is just so much easier! I even found a mythtv channel. All in all I am real happy with the Roku box and would recomend it to anyone to supplement their basic cable package. I think it really goes hand in hand with a basic cable or HD Antenna where you can get your network channels, but still get alot of the extra content for free or a reasonable price. 

I will say that I would like to try Hulu Plus and see if I can slowly ween myself from Cable or HD Antenna. I really doubt it because of the way the cable companies are in the US. Right now since I get my internet from the cable company(Charter) if I get basic Digital television with it then I actually get the two of them for a cheaper price than if I just got the cable internet from them. I would like to end by saying that I know I have only mentioned a few of the channels that roku offers but there really are so many of them and they are scattered in many places that it is tough to know about them all. So if you want you can email me james.middendorff@gmail.com I am on google+ as well. 


Thanks]]>
</description>
    <itunes:summary><![CDATA[
I recently bought a roku XD box and I want to do a little review for you guys. First I would like to say that we have basic cable, and werent really looking to become &quot;cord cutters&quot;. You can check out the roku site at www.roku.com.




First I bought it from Best buy for 79.99 I think it was 84 dollars with tax. First I cant believe how tiny this thing is! When you open the box, you have the device, a remote and some RCA AV Cables. The XD also only works with wireless internet. The one I bought does have a HDMI port on it. It works great and does Netflix and other services like Amazon and Hulu Plus. 


You sign up for a roku account on the website and associate the roku box to that account. Which is close to what you do with most media type boxes now a days like blueray players and stuff. The only difference is those devices only give you content from a couple places. The roku actually has channels, that you can add to your roku box. I have added many channels on it and I am watching lots of content from the web. With services like Popcorn flix, which shows you movies and during those movies there are some commercials. They arent really that bad its like 1 commercial when they show it. It is always the same commercial though. Which kinda gets annoying. 


I use Netflix, and Crackle on it was well. There are lots of news channels like NBC also I know that MLB and I think NHL have channels too. I was amazed at the selection of channels on the device. It would be nice if the roku site had a listing of all private channels. As it is hard to search the internet to find a list, then add the code on the roku site only to find out that the channel code doesnt work anymore. There really are so many channels that you can add it is hard to explain them all. 


My wife and I both have android phones and we installed the roku remote control app which works great! As long as you are on the same wifi network as the roku you can control it. My wife is using the roku box for netflix and other services more than our blueray players because she likes the interface to netflix better. She says that just getting around the netflix channel is just so much easier! I even found a mythtv channel. All in all I am real happy with the Roku box and would recomend it to anyone to supplement their basic cable package. I think it really goes hand in hand with a basic cable or HD Antenna where you can get your network channels, but still get alot of the extra content for free or a reasonable price. 

I will say that I would like to try Hulu Plus and see if I can slowly ween myself from Cable or HD Antenna. I really doubt it because of the way the cable companies are in the US. Right now since I get my internet from the cable company(Charter) if I get basic Digital television with it then I actually get the two of them for a cheaper price than if I just got the cable internet from them. I would like to end by saying that I know I have only mentioned a few of the channels that roku offers but there really are so many of them and they are scattered in many places that it is tough to know about them all. So if you want you can email me james.middendorff@gmail.com I am on google+ as well. 


Thanks]]>
</itunes:summary>
    <pubDate>Sun, 25 Mar 2012 00:00:00 +0000</pubDate>
    <enclosure url="http://hackerpublicradio.org/eps/hpr0951.mp3" length="4047101" type="audio/mpeg"/>
    <guid>http://hackerpublicradio.org/eps/hpr0951.mp3</guid>
    <itunes:explicit>yes</itunes:explicit>
  </item>

  </channel>
</rss>

