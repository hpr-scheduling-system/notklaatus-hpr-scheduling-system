#!/bin/bash
# time for mediafile in intro.mp3 mr-gadgets-ham-radio.wav outro.mp3;do ffmpeg -i $mediafile -ar 44100 -ab 128k -ac 1 ${mediafile%.*}-intermediate.wav;done 
# sox -S intro-intermediate.wav mr-gadgets-ham-radio-intermediate.wav outro-intermediate.wav  -c 1 -r 44100 out.ogg
# sox -S intro-intermediate.wav mr-gadgets-ham-radio-intermediate.wav outro-intermediate.wav  -c 1 -r 44100 out.mp3
# sox -S intro-intermediate.wav mr-gadgets-ham-radio-intermediate.wav outro-intermediate.wav  -c 1 -r 16000 -t wav - | speexenc - out.spx

# ffmpeg -i mr-gadgets-ham-radio.wav -ar 44100 -ab 128 -ac 1 -acodec mp3
# for mediafile in intro.mp3 mr-gadgets-ham-radio.wav outro.mp3;do ffmpeg -i $mediafile -ar 44100 -ab 128k -ac 1 ${mediafile%.*}-intermediate.mp3;done ; sox -S *intermediate* -c 1 -r 44100 out.mp3 ; normalize-audio -a 0.6 out.mp3
################################################################################
#
# script to prepare audio files for HPR shows
#
# input: mp3 or ogg file
# result: mp3, ogg in 44100 Hz, spx files 16000Hz with intro and outro
# provides 3 interactive checks for audio quality, intro and outro
#
################################################################################
# This file is part of prep_audio
# 
# prep_audio is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# prep_audio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with prep_audio.  If not, see <http://www.gnu.org/licenses/>.
# http://www.gnu.org/licenses/agpl-3.0.html
# #########################################################################

################################################################################
#
# PREREQUISITS
# - current folder has to be writable
# - there should NOT be a temp.ogg or temp.mp3 file
# - intro.mp3 and outro.mp3 have to be present
# - IMPORTANT: sox compiled with mp3 support
#                see http://a0u.xanga.com/700438974/howto-soc-installation
#
#
# IMPORTANT
# Backup the files before feeding them to this script, no guarantees here
# Handling of .wav not yet tested, but it should work 
#
# code.cruncher, May 2011
# 
################################################################################

################################################################################
#
# TODO
# 
# test handling of wav files
# add play final files or open them in specific player(s)
# add handling of ID3 tags
# create html interface for standardized info gathering 
#
################################################################################


# CHECK ARGUMENTS  -------------------------------------------------------------
usage="usage: $0 <fname>, fname is an mp3, ogg or wav file with extension mp3, ogg or wav"

# if not ${mediafile} return usage
if [ $# -ne 1 ]; then
  echo $usage
  exit 1
fi

mediafile=${1}

# test if file exists 
if [ ! -f "${mediafile}" ]; then
  echo "sorry, file \"${mediafile}\" does not exist"
  exit 1
fi

# test if file exists 
if [ ! -r "${mediafile}" ]; then
  echo "sorry, file \"${mediafile}\" is not readable"
  exit 1
fi

# extract file name and extension
fname=${mediafile%.*}
ext=${mediafile/*./}

#Make a backup
cp -v ${mediafile} ${mediafile}_$(md5sum ${mediafile} | cut -c -32 ).orig

if [[ ! -e ${mediafile}_$(md5sum ${mediafile} | cut -c -32 ).orig ]]; then
  echo "Backup not made: ${mediafile}_$(md5sum ${mediafile} | cut -c -32).orig"
  exit
fi

# check if extension = mp3 or ogg 
if [[ $ext != "mp3" && $ext != "ogg" && $ext != "wav" && $ext != "flac" ]]; then
  echo "sorry, file \"${mediafile}\" is not an mp3, ogg, wav, or flac file"
  exit 1
fi

# CHECK FILE --------------------------------------------------------------------
# check for 44100 Hz and correct if necessary
if [ $(eval "soxi -r \"${mediafile}\"") != 44100 -o $(eval "soxi -c \"${mediafile}\"") != 1 ]; then
  NOT_TO_SPEC=1
  orig_rate=$(eval "soxi -r \"${mediafile}\"")
fi

# check audio quality
dur=7			# playtime of sample in seconds
go=1			# variable to repeat playing of sample
from=180                # start sample at 3 minutes in

while [ $go -ne 0 ]
do
  echo
  echo "--------------------------------------------------------------------------------"
  echo "1/3 AUDIO TEST: check audio quality: ... playing $dur seconds ..." 
  play "${mediafile}" trim $from $dur
  ((from+=180))           # next sample will be 3 minutes later
  read -s -n1 -p "sound quality ok?[y,n] ... or play another sample[a] ... [y,n,a]"
  echo
  case "$REPLY" in
    n) echo "aborting ... get better quality sound file ... good bye!"; exit 0;;
    y) go=0;;
  esac
done

# Check for intro
echo
echo "--------------------------------------------------------------------------------"
echo "2/3 INTRO TEST: Is the intro playing? "
play "${mediafile}" trim 1 5	# play 5 seconds at beginning of file
read -s -n1 -p "intro ok? [y, n]"; echo
if [ $REPLY = 'n' ]; then
  INTRO="intro_tmp.$ext"
  eval "cp -fv \"intro.mp3\" \"$INTRO\""
fi

# Check for outro
echo 
echo "--------------------------------------------------------------------------------"
echo "3/3 OUTRO TEST: Is the outro playing? "
len=$(eval "soxi -D \"${mediafile}\"")
len=$(echo "scale=0; $len - 50" | bc)
play "${mediafile}" trim $len 5
read -s -n1 -p "outro ok? [y, n]"; echo
if [ $REPLY = 'n' ]; then
  OUTRO="outro_tmp.$ext"
  eval "cp -fv \"outro.mp3\" \"$OUTRO\""
fi

# CREATE MP3, OGG, and SPX  ----------------------------------------------------------

#convert to 44100 single channel if necessary
if [[ $NOT_TO_SPEC ]]; then  
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting ${mediafile} from $orig_rate to 44100."
  eval "mv -fv ${mediafile} \"tmp_${mediafile}\""
  eval "sox -S \"tmp_${mediafile}\" -c 1 -r 44100 \"${mediafile}\"" 
  if [[ ! -e ${mediafile} ]]; then
    echo "Processing error: ${mediafile}"
    exit
  fi
  eval "rm -fv \"tmp_${mediafile}\""
fi

#Add intro/outro if necessary
if [[ $INTRO != "" || $OUTRO != "" ]]; then 
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Combining ${INTRO} tmp_${mediafile} ${OUTRO}"
  eval "mv -fv ${mediafile} \"tmp_${mediafile}\""
#  eval "sox -S \"$INTRO\" \"tmp_${mediafile}\" \"$OUTRO\" -c 1 -r 44100 \"${mediafile}\"" 
  sox -S $INTRO tmp_${mediafile} $OUTRO -c 1 -r 44100 ${mediafile} 
  if [[ ! -e ${mediafile} ]]; then
    echo "Processing error: ${mediafile}"
    exit
  fi
  #delete tmp_${mediafile}, $INTRO, and $OUTRO files
  eval "rm -fv \"tmp_${mediafile}\""
  eval "rm -fv \"$INTRO\""
  eval "rm -fv \"$OUTRO\""
fi

#normalize-audio -a 0.6
# Create all 3 output formats, mp3, ogg, and spx
if [ $ext = "ogg" ]; then 
  if [ -e ${mediafile}.mp3 ]
  then
    echo "Skipping transcoding of the mp3 as the file ${mediafile}.mp3 exists."
  else
    echo 
    echo "--------------------------------------------------------------------------------"
    echo "Converting: ogg file to mp3."
    normalize-ogg -a 0.6 ${mediafile}
    eval "sox -S \"${mediafile}\" -c 1 -r 44100 \"${1/ogg/mp3}\""
    echo 
    echo "--------------------------------------------------------------------------------"
    echo "Converting: ogg file to spx."
    eval "sox -S \"${mediafile}\" -c 1 -r 16000 -t wav  - | speexenc - \"${1/ogg/spx}\""
  fi
elif [ $ext = "mp3" ]; then
  if [ -e ${mediafile}.ogg ]
  then
    echo "Skipping transcoding of the ogg as the file ${mediafile}.ogg exists."
  else
    echo 
    echo "--------------------------------------------------------------------------------"
    echo "Converting: mp3 file to ogg."
    normalize-audio -a 0.6 ${mediafile}
    eval "sox -S \"${mediafile}\" -c 1 -r 44100 \"${1/mp3/ogg}\""
    echo 
    echo "--------------------------------------------------------------------------------"
    echo "Converting: mp3 file to spx."
    eval "sox -S \"${mediafile}\" -c 1 -r 16000 -t wav - | speexenc - \"${1/mp3/spx}\""
  fi
elif [ $ext = "flac" ]; then
  normalize-audio -a 0.6 ${mediafile}
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting: flac file to mp3."
  eval "sox -S \"${mediafile}\" -c 1 -r 44100 \"${1/flac/mp3}\""
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting: flac file to ogg."
  eval "sox -S \"${mediafile}\" -c 1 -r 44100 \"${1/flac/ogg}\""
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting: flac file to spx."
  eval "sox -S \"${mediafile}\" -c 1 -r 16000 -t wav - | speexenc - \"${1/flac/spx}\""              
elif [ $ext = "wav" ]; then
  normalize-audio -a 0.6 ${mediafile}
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting: wav file to mp3."
  eval "sox -S \"${mediafile}\" -c 1 -r 44100 \"${1/wav/mp3}\""
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting: wav file to ogg."
  eval "sox -S \"${mediafile}\" -c 1 -r 44100 \"${1/wav/ogg}\""
  echo 
  echo "--------------------------------------------------------------------------------"
  echo "Converting: wav file to spx."
  eval "sox -S \"${mediafile}\" -c 1 -r 16000 -t wav - | speexenc - \"${1/wav/spx}\""					     
fi